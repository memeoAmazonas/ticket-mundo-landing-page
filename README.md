  <h3 align="center">Landing page Ticketmundo</h3>

  <p align="center">
    <a href="https://ticketmundo.com/">
      <img src="logo.png" alt="Logo" width="160" height="35">
    </a>
    <br />
    <a href="https://ticketmundo.com/">View Site</a>
  </p>


<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#Installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#develop-mode">Develop mode</a></li>
    <li><a href="# production-mode-and-upload-to-server"> production mode and upload to server</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>



<!-- GETTING STARTED -->
## Getting Started

### Prerequisites

* node version   v10.23.0 or more
* npm
  ```sh
  npm install npm@latest -g
  ```

### Installation

1. Clone the repo
   ```sh
   git clone  https://gitlab.com/memeoAmazonas/ticket-mundo-landing-page.git
   ```
2. Install NPM packages
   ```sh
   npm install
   ```
<!-- DEVELOP MODE -->
## develop-mode
 ```sh
   npm run dev
   ```
## production-mode-and-upload-to-server
<strong>remember to modify the config.js depending on each country</strong>
 ```sh
   npm run production
   ```
<p>this command creates a dist directory, which must be copied to the server, the favicon or conf.xml must not be removed from the server</p>

<!-- CONTACT -->
## Contact

José Ortíz 
- <a href="mailto:proyectosjgot@gmail.com"><img src="https://img.shields.io/badge/gmail-%23DD0031.svg?&style=for-the-badge&logo=gmail&logoColor=white"/></a>
- [twitter](https://twitter.com/ajjicero) 
- [github](https://github.com/memeoAmazonas) 
             
