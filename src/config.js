import { keys } from 'lodash';

const countries = require('./static/data/countries.json');

const codeChile = 'CL';
const codeVenezuela = 'VE';
const codeEstadosUnidos = 'US';
const codeEuropa = keys(countries.europe);

const config = {
  idCountry: 2,
  countryCode: codeEuropa,
  urlServices: 'https://apitm.ticketmundo.com/api',
  urlCdnImages: 'https://ticketmundoblob.blob.core.windows.net',
  urlFront: '*',
  itemsSearch: 9,
  defaultLang: 'es',
  languageList: [
    {
      id: 'language.spanish',
      lang: 'es',
    },
    {
      id: 'language.english',
      lang: 'en',
    },
  ],
  section: [
    {
      name: 'CarouselContent',
      visible: true,
    },
    {
      name: 'MultiCarousel',
      visible: true,
    },
    {
      name: 'Event',
      visible: true,
    },
    {
      name: 'FindShow',
      visible: false,
    },
    {
      name: 'TkPlay',
      visible: true,
    },
    {
      name: 'AboutUs',
      visible: true,
    },
  ],
  minItemsMultiCarousel: 4,
  socialMediaLink: [
    {
      name: 'facebook',
      link: 'https://www.facebook.com/ticketmundo/',
      src: '',
    },
    {
      name: 'twitter',
      link: 'https://twitter.com/Ticketmundo_ve',
      src: '',
    },
    {
      name: 'instagram',
      link: 'https://www.instagram.com/ticketmundo_ve/',
      src: '',
    },
    {
      name: 'youtube-play',
      link: 'https://www.youtube.com/channel/UC6a6_jp6JvdqIQLjWXNcPmg',
      src: '',
    },
  ],
  urlSites: [
    {
      countryRegion: 'Estados Unidos',
      link: 'ticketmundo.com/#',
    },
    {
      countryRegion: 'Chile',
      link: 'ticketmundo.cl/#',
    },
    {
      countryRegion: 'Venezuela',
      link: 've.ticketmundo.com/#',
    },
    {
      countryRegion: 'Europa',
      link: 'ticketmundo.eu/#',
    },
  ],
  contactUsNumberPhone: '2-2588 6699',
};
export default config;
