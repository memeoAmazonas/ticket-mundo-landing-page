import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route } from 'react-router-dom';
import regeneratorRuntime from 'regenerator-runtime';


import Page from '@/views/layout/page';
import history from './history';
import configureStore from './redux/store/store';

import registerServiceWorker from './registerServiceWorker';

import '@/sass/style.scss';

if (sessionStorage.country) {
  sessionStorage.clear();
}

document.addEventListener('touchstart', () => null, { passive: true });
const store = configureStore();
ReactDOM.render(
  <Router history={history}>
    <Provider store={store}>
      <Route component={Page} />
    </Provider>
  </Router>,
  document.getElementById('app'),
);

if (module.hot) {
  module.hot.accept();
}
registerServiceWorker();
