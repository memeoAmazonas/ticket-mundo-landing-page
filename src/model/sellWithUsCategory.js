import aggregate from '../static/images/sell-wit-us/categories-icon/aggregates-icon.svg';
import marketing from '../static/images/sell-wit-us/categories-icon/marketing-icon.svg';
import streaming from '../static/images/sell-wit-us/categories-icon/streaming-icon.svg';
import ticketIn from '../static/images/sell-wit-us/categories-icon/ticketing-icon.svg';

export const sellCategory = [
  {
    label: 'sell.with.us.ticketin',
    src: ticketIn,
  },
  {
    label: 'sell.with.us.aggregate',
    src: aggregate,
  },
  {
    label: 'sell.with.us.streaming',
    src: streaming,
  },
  {
    label: 'sell.with.us.marketing',
    src: marketing,
  },
];
export const itemListSellWithUs = [
  'sell.with.us.sell.ticket.list.0',
  'sell.with.us.sell.ticket.list.1',
  'sell.with.us.sell.ticket.list.2',
];

export const itemListBroadcast = [
  'sell.with.us.sell.broadcast.list.0',
  'sell.with.us.sell.broadcast.list.1',
  'sell.with.us.sell.broadcast.list.2',
]
