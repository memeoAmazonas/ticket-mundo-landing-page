import {Link} from "../interface/Link";
import config from '../config';
const { socialMediaLink  } = config;
const SocialMediaList: Link[] = socialMediaLink;
export default SocialMediaList;
