import agotado from '@/static/images/sold-out-icon.svg';
import Gratuito from '@/static/images/free-icon.svg';
import ListaEspera from '@/static/images/wait-list-icon.svg';
import RecienAnunciado from '@/static/images/just-announced-icon.svg';

export {
  Gratuito, ListaEspera, agotado, RecienAnunciado,
};
