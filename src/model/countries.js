import { keys, merge } from 'lodash';
import eeuu from '@/static/images/flag-eeuu.svg';
import chile from '@/static/images/flag-chile.svg';
import europe from '@/static/images/flag-europe.svg';
import vzla from '@/static/images/flag-venezuela.svg';

import config from '../config';

const codes = require('@/static/data/countries.json');

const counstriesCode = {
  europa: keys(codes.europe),
};

const { urlSites } = config;
const ListCountry = [
  {
    countryCode: 'US',
    name: 'country.eeuu',
    src: eeuu,
    width: 11,
  },
  {
    countryCode: 'CL',
    name: 'country.chile',
    src: chile,
    width: 9.6,
  },
  {
    countryCode: 'VE',
    name: 'country.venezuela',
    src: vzla,
    width: 18.1,
  },
  {
    countryCode: counstriesCode.europa,
    name: 'country.europe',
    src: europe,
    width: 10.1,
  },
];
const Countries = () => ListCountry.map((item, index) => merge(item, urlSites[index]));
export default Countries();
