import it1 from '../static/images/contact-us/items/item-1.svg';
import it2 from '../static/images/contact-us/items/item-2.svg';
import it3 from '../static/images/contact-us/items/item-3.svg';
import it4 from '../static/images/contact-us/items/item-4.svg';
import it5 from '../static/images/contact-us/items/item-5.svg';
import youtube from '../static/images/contact-us/social-icons/youtube-icon.svg';
import twiter from '../static/images/contact-us/social-icons/twitter-icon.svg';
import instagram from '../static/images/contact-us/social-icons/instagram-icon.svg';
import facebook from '../static/images/contact-us/social-icons/facebook-icon.svg';
import eventIcon from '../static/images/contact-us/detail/events-icon.png';
import covid from '../static/images/contact-us/detail/covid_19-icon.png';
import services from '../static/images/contact-us/detail/services-icon.png';
import refunds from '../static/images/contact-us/detail/refunds-icon.png';
import support from '../static/images/contact-us/detail/support-icon.png';

import config from '../config';

const { socialMediaLink } = config;
export const contactUsItems = [
  {
    title: 'contact.us.item.title',
    content: 'contact.us.item.content',
    src: it1,
    to: 'covid-19',
  },
  {
    title: 'event',
    content: 'contact.us.item.content1',
    src: it2,
    to: 'events',
  },
  {
    title: 'contact.us.item.title2',
    content: 'contact.us.item.content2',
    src: it3,
    to: 'services',
  },
  {
    title: 'contact.us.item.title3',
    content: 'contact.us.item.content3',
    src: it4,
    to: 'Refunds-and-moving',
  },
  {
    title: 'contact.us.item.title4',
    content: 'contact.us.item.content4',
    src: it5,
    to: 'phone-sales-support',
  },
];


export const socialIconsContactUs = [
  {
    icon: facebook,
    url: socialMediaLink[0].link,
  },
  {
    icon: twiter,
    url: socialMediaLink[1].link,
  },
  {
    icon: instagram,
    url: socialMediaLink[2].link,
  },
  {
    icon: youtube,
    url: socialMediaLink[3].link,
  },
];

export const detailContactUs = [
  {
    id: 'covid-19',
    label: 'contact.us.item.title',
    subtitle: 'contact.us.item.content',
    icon: covid,
    items: [
      {
        label: 'contact.us.detail.covid.item.label',
        content: 'contact.us.detail.covid.item.content',
      },
      {
        label: 'contact.us.detail.covid.item1.label',
        content: 'contact.us.detail.covid.item1.content',
      },
    ],
  },
  {
    id: 'services',
    label: 'contact.us.item.title2',
    subtitle: 'contact.us.item.content2',
    icon: services,
    items: [
      {
        label: 'contact.us.detail.services.item.label',
        content: 'contact.us.detail.services.item.content',
      },
      {
        label: 'contact.us.detail.services.item1.label',
        content: 'contact.us.detail.services.item1.content',
      },
    ],
  },
  {
    id: 'events',
    label: 'event',
    subtitle: 'contact.us.item.content1',
    icon: eventIcon,
    items: [
      {
        label: 'contact.us.detail.events.item.label',
        content: 'contact.us.detail.events.item.content',
      },
      {
        label: 'contact.us.detail.events.item1.label',
        content: 'contact.us.detail.events.item1.content',
      },
      {
        label: 'contact.us.detail.events.item2.label',
        content: 'contact.us.detail.events.item2.content',
      },
      {
        label: 'contact.us.detail.events.item3.label',
        content: 'contact.us.detail.events.item3.content',
      },
      {
        label: 'contact.us.detail.events.item4.label',
        content: 'contact.us.detail.events.item4.content',
      },
      {
        label: 'contact.us.detail.events.item5.label',
        content: 'contact.us.detail.events.item5.content',
      },
      {
        label: 'contact.us.detail.events.item6.label',
        content: 'contact.us.detail.events.item6.content',
      },
      {
        label: 'contact.us.detail.events.item7.label',
        content: 'contact.us.detail.events.item7.content',
      },
    ],
  },
  {
    id: 'Refunds-and-moving',
    label: 'contact.us.item.title3',
    subtitle: 'contact.us.item.content3',
    icon: refunds,
    items: [
      {
        label: 'contact.us.detail.refunds.item.label',
        content: 'contact.us.detail.refunds.item.content',
        content1: 'contact.us.detail.refunds.item.content1',
      },
      {
        label: 'contact.us.detail.refunds.item1.label',
        content: 'contact.us.detail.refunds.item1.content',
        content1: 'contact.us.detail.refunds.item1.content1',
      },
      {
        label: 'contact.us.detail.refunds.item2.label',
        content: 'contact.us.detail.refunds.item2.content',
      },
    ],
  },
  {
    id: 'phone-sales-support',
    label: 'contact.us.item.title4',
    subtitle: 'contact.us.item.content4',
    icon: support,
    items: [
      {
        label: 'contact.us.detail.support.item.label',
        content: 'contact.us.detail.support.item.content',
      },
      {
        label: 'contact.us.detail.support.item1.label',
        content: 'contact.us.detail.support.item1.content',
      },
      {
        label: 'contact.us.detail.support.item2.label',
        content: 'contact.us.detail.support.item2.content',
      },
    ],
  },
];
export const idsDetailContactUs = ['events', 'covid-19', 'services', 'Refunds-and-moving', 'phone-sales-support'];
