import {AboutUsItem} from "../interface/aboutUsItem";

const AboutUsListCategory: AboutUsItem[]=[
  {
    label: "aboutus.title",
    items: [
      {
        external: false,
        label: "aboutus.title.branch.resource",
        url: '/brand-resources',
      },
      {
        external: false,
        label: "aboutus.title.sell.withus",
        url: '/sell-with-us',
      },
    ]
  },
  {
    label: "aboutus.tools",
    items: [
      {
        external: true,
        label: "aboutus.tools.control.panel",
        url: 'https://admin.ticketmundo.com/',
      },
      {
        external: true,
        label: "aboutus.tools.ticket.soft",
        url: 'https://boxoffice.ticketmundo.com/',
      },
    ]
  },
  {
    label: "aboutus.more",
    items: [
      {
        external: true,
        label: "ticketPlay",
        url: 'https://play.ticketmundo.com/',
      },
    ]
  },
  {
    label: "aboutus.accounts",
    items: [
      {
        external: false,
        label: "aboutus.accounts.myaccount",
        url: '',
      },
      {
        external: false,
        label: "aboutus.accounts.create.account",
        url: '',
      },
      {
        external: false,
        label: "language",
        url: '',
      }
]
}
];
export default AboutUsListCategory;
/*
const AboutUsListCategory: AboutUsItem[]=[
  {
    label: "aboutus.title",
    items: [
      {
        external: false,
        label: "aboutus.title.abouts",
        url: '',
      },
      {
        external: false,
        label: "aboutus.title.branch.resource",
        url: '/brand-resources',
      },
      {
        external: false,
        label: "aboutus.title.sell.withus",
        url: '/sell-with-us',
      },
    ]
  },
  {
    label: "aboutus.tools",
    items: [
      {
        external: false,
        label: "aboutus.tools.control.panel",
        url: '',
      },
      {
        external: false,
        label: "aboutus.tools.ticket.soft",
        url: '',
      },
      {
        external: false,
        label: "aboutus.tools.app.control.access",
        url: '',
      },
    ]
  },
  {
    label: "aboutus.more",
    items: [
      {
        external: true,
        label: "ticketPlay",
        url: 'https://play.ticketmundo.com/',
      },
      {
        external: true,
        label: "fantempo",
        url: '',
      }
    ]
  },
  {
    label: "aboutus.accounts",
    items: [
      {
        external: false,
        label: "aboutus.accounts.myaccount",
        url: '',
      },
      {
        external: false,
        label: "aboutus.accounts.create.account",
        url: '',
      },
      {
        external: false,
        label: "language",
        url: '',
      }
]
}
];
export default AboutUsListCategory;
*/

