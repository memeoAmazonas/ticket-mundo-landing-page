import React from 'react';

export const IconMobileSearch = ({ color }) => (
  <svg xmlns="http://www.w3.org/2000/svg" width="17.06" height="17.059" viewBox="0 0 17.06 17.059">
    <path
      id="Trazado_3859"
      data-name="Trazado 3859"
      d="M331.265,220.553l-3.078-2.977a7.327,7.327,0,1,0-.7.691l3.095,2.993a.492.492,0,1,0,.684-.707Zm-14.925-7.861a6.368,6.368,0,1,1,6.368,6.367A6.376,6.376,0,0,1,316.34,212.691Z"
      transform="translate(-314.856 -204.84)"
      fill={color}
      stroke={color}
      strokeWidth="1"
    />
  </svg>

);

export const IconDeskSearch = ({ color }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="20"
    height="20"
    viewBox="0 0 20 20"
  >
    <g id="Grupo_3811" data-name="Grupo 3811" transform="translate(19746 -2733)">
      <path
        id="Trazado_24232"
        data-name="Trazado 24232"
        d="M331.524,220.8l-3.128-3.025a7.447,7.447,0,1,0-.712.7l3.145,3.042a.5.5,0,1,0,.695-.719Zm-15.168-7.989a6.472,6.472,0,1,1,6.472,6.471A6.479,6.479,0,0,1,316.356,212.811Z"
        transform="translate(-20059.355 2529.66)"
        fill={color}
      />
      <rect
        id="Rectángulo_3718"
        data-name="Rectángulo 3718"
        width="20"
        height="20"
        transform="translate(-19746 2733)"
        fill="none"
      />
    </g>
  </svg>
);

export const iconMenuOpen = {
  height: '16.588',
  viewBox: '0 0 22 16.588',
  width: '22',
  type: 'path',
  transform: 'translate(-382.911 -91.244)',
  path: [
    {
      d: 'M0,0V10',
      transform: 'translate(393.911 99.538) rotate(-90)',
      fill: 'none',
      strokeLinecap: 'round',
      strokeWidth: '2',
    },
    {
      d: 'M0,0V10',
      transform: 'translate(393.911 106.832) rotate(-90)',
      fill: 'none',
      strokeLinecap: 'round',
      strokeWidth: '2',
    },
    {
      d: 'M0,9.2v20',
      transform: 'translate(374.712 92.244) rotate(-90)',
      fill: 'none',
      strokeLinecap: 'round',
      strokeWidth: '2',
    },
  ],
};

export const iconMenuClose = {
  width: '22',
  height: '16.588',
  viewBox: '0 0 22 16.588',
  transform: 'translate(-382.911 -91.244)',
  type: 'path',
  path: [
    {
      d: 'M0,0V20',
      transform: 'translate(383.911 99.538) rotate(-90)',
      fill: 'none',
      strokeLinecap: 'round',
      strokeWidth: '2',
    },
    {
      d: 'M0,0V20',
      transform: 'translate(383.911 106.832) rotate(-90)',
      fill: 'none',
      strokeLinecap: 'round',
      strokeWidth: '2',
    },
    {
      d: 'M0,9.2V20',
      transform: 'translate(383.911 92.244) rotate(-90)',
      fill: 'none',
      strokeLinecap: 'round',
      strokeWidth: '2',
    },
  ],
};

export const iconCloseWindow = {

  width: '18.121',
  height: '18.121',
  viewBox: '0 0 18.121 18.121',
  type: 'line',
  transform: 'translate(-388.343 -242.301)',
  path: [
    {
      x2: '16',
      y2: '16',
      transform: 'translate(389.403 243.362)',
      fill: '#070707',
      strokeLinecap: 'round',
      strokeLinejoin: 'round',
      strokeWidth: '1.5',
      stroke: '#919baf',
    },
    {

      y1: '16',
      x2: '16',
      transform: 'translate(389.403 243.362)',
      fill: '#070707',
      stroke: '#919baf',
      strokeLinecap: 'round',
      strokeLinejoin: 'round',
      strokeWidth: '1.5',
    }],
};
export const ContacUsIcon = ({ color }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="30"
    height="20"
    viewBox="0 0 31.137 20.568"
  >
    <g id="Grupo_2574" data-name="Grupo 2574" transform="translate(-317.441 -137.072)">
      <path
        id="Trazado_3348"
        data-name="Trazado 3348"
        d="M318.637,150.768a14.159,14.159,0,0,1,27.856,0"
        transform="translate(0.444 1.273)"
        fill="none"
        stroke={color}
        strokeMiterlimit="10"
        strokeWidth="1"
      />
      <path
        id="Trazado_3349"
        data-name="Trazado 3349"
        d="M347.453,146l.566,5.1H318l.566-5.1Z"
        transform="translate(0 6.045)"
        fill="none"
        stroke={color}
        strokeMiterlimit="10"
        strokeWidth="1"
      />
      <line
        id="Línea_582"
        data-name="Línea 582"
        x2="7.148"
        transform="translate(329.436 137.572)"
        fill="none"
        stroke={color}
        strokeMiterlimit="10"
        strokeWidth="1"
      />
      <line
        id="Línea_583"
        data-name="Línea 583"
        y1="3.097"
        transform="translate(333.01 137.333)"
        fill="none"
        stroke={color}
        strokeMiterlimit="10"
        strokeWidth="1"
      />
    </g>
  </svg>
);
export const IconCloseWindow = ({ width, height }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width={width}
    height={height}
    viewBox="0 0 18.121 18.121"
  >
    <g id="Grupo_3700" data-name="Grupo 3700" transform="translate(-388.343 -242.301)">
      <line
        id="Línea_613"
        data-name="Línea 613"
        x2="16"
        y2="16"
        transform="translate(389.403 243.362)"
        fill="#070707"
        stroke="#919baf"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.5"
      />
      <line
        id="Línea_614"
        data-name="Línea 614"
        y1="16"
        x2="16"
        transform="translate(389.403 243.362)"
        fill="#070707"
        stroke="#919baf"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.5"
      />
    </g>
  </svg>

);
