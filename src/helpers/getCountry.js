import { filter, isEqual } from 'lodash';

export default (country, countryList) => {
  if (typeof countryList !== 'string' && typeof country === 'string') {
    const item = filter(countryList, (it) => it.toUpperCase() === country.toUpperCase());
    return item.length > 0;
  }

  if (typeof countryList !== 'string' && typeof country !== 'string') {
    return isEqual(country, countryList);
  }
  if (typeof countryList === 'string' && typeof country === 'string') {
    return isEqual(country.toUpperCase(), countryList.toUpperCase());
  }
  return false;
};
