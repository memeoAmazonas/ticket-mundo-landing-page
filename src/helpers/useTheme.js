import { useEffect, useState } from 'react';

export default () => {
  const [theme, setTheme] = useState('light');
  const toggleTheme = () => {
    if (theme !== 'dark') {
      sessionStorage.setItem('theme', 'dark');
      setTheme('dark');
    } else {
      sessionStorage.setItem('theme', 'light');
      setTheme('light');
    }
  };

  useEffect(() => {
    const localTheme = sessionStorage.getItem('theme');
    if (localTheme) {
      setTheme(localTheme);
    }
  }, []);

  return {
    theme,
    toggleTheme,
  };
};
