export default (width, first, second, third, fourth) => {
  if (width.width >= 3200) {
    return first;
  }
  if (width.width > 768 && width.width < 3200) {
    return second;
  }
  if (width.width > 414 && width.width <= 768) {
    return third;
  }
  if (width.width <= 414) {
    return fourth;
  }
  return 0;
};
