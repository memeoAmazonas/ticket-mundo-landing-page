import { filter, isEqual, keys } from 'lodash';
import logoChile from '@/static/images/logo/light/logo-chile.svg';
import logoChileDark from '@/static/images/logo/dark/logo-chile.svg';
import logoVenezuela from '@/static/images/logo/light/logo-venezuela.svg';
import logoVenezuelaDark from '@/static/images/logo/dark/logo-venezuela.svg';
import logoEeuu from '@/static/images/logo/light/logo-eeuu.svg';
import logoEeuuDark from '@/static/images/logo/dark/logo-eeuu.svg';
import logoEuropa from '@/static/images/logo/light/logo-europa.svg';
import logoEuropaDark from '@/static/images/logo/dark/logo-europa.svg';
import logoDefault from '@/static/images/logo-principal.svg';
import logoDefaultDark from '@/static/images/logo-principal-dark.svg';
import config from '../config';

const codes = require('../static/data/countries.json');

export const logos = [
  {
    id: 'VE',
    isDark: false,
    src: logoVenezuela,
  },
  {
    id: 'VE',
    isDark: true,
    src: logoVenezuelaDark,
  },
  {
    id: 'CL',
    isDark: false,
    src: logoChile,
  },
  {
    id: 'CL',
    isDark: true,
    src: logoChileDark,
  },
  {
    id: 'US',
    isDark: false,
    src: logoEeuu,
  },
  {
    id: 'US',
    isDark: true,
    src: logoEeuuDark,
  },
  {
    id: keys(codes.europe),
    isDark: false,
    src: logoEuropa,
  },
  {
    id: keys(codes.europe),
    isDark: true,
    src: logoEuropaDark,
  },
  {
    id: 'default',
    isDark: false,
    src: logoDefault,
  },
  {
    id: 'default',
    isDark: true,
    src: logoDefaultDark,
  },
];

export const getLogo = (isDark) => {
  const logo = filter(logos,
    (item) => isEqual(item.id, config.countryCode) && item.isDark === isDark);
  try {
    return logo[0].src;
  } catch (e) {
    if (isDark) {
      return logoDefaultDark;
    }
    return logoDefault;
  }
};
