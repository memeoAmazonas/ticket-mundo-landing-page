import { getCookie, getSessionItem } from './setCookies';

const setHeightView = (height, width) => {
  const compare = getCookie('setCookie') || getSessionItem('cookie');
  let diference = 0;
  if (!compare) {
    if (width > 722) {
      diference = 154 + 398;
    }
    if (width > 590 && width <= 722) {
      diference = 160 + 398;
    }
    if (width <= 590) {
      diference = 195 + 398;
    }
  } else if (width > 722) {
    diference = 109 + 398;
  } else {
    diference = 115 + 398;
  }
  return height - diference;
};
export default setHeightView;
