import { concat } from 'lodash';

import config from '@/config';

const validateClose = (target) => {
  const { languageList } = config;
  const ids = [];
  let response = true;
  target.forEach((item) => {
    if (item.id !== '' && item.id !== undefined && item.id !== 'undefined') {
      ids.push(item.id);
    }
  });
  let languages = languageList.map((item) => item.lang);
  languages = concat(languages, ['set-language', 'set-language-button']);
  ids.forEach((item) => { if (languages.includes(item)) response = false; });
  return response;
};
export default validateClose;
