export const validate = (value, pattern) => {
  if (!pattern) return null;
  if (typeof pattern === 'string' && !(new RegExp(pattern, 'g').test(value))) {
    return false;
  }
  if (typeof pattern === 'object') {
    const patterns = pattern.map((rule) => new RegExp(rule, 'g'));
    for (let i = 0; i < pattern.length; i += 1) {
      if (!patterns[i].test(value)) {
        return false;
      }
    }
  }
  return true;
};
