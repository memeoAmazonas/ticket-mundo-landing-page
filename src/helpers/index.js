import getSizeWindow from './getSizeWindow';
import RedirectTo from './redirectTo';
import UseComponentVisible from './useComponentVisible';
import setNumberItem from './setNumberItem';
import useDocumentScrollThrottled from './useDocumentScroll';
import getCountry from './getCountry';
import ScrollToTop from './scrollToTop';
import setHeightView from './setHeighView';
import setShadow from './setShadowItem';

export * from './setCookies';
export * from './getLogo';
export * from './darkMode';
export * from './setDataItems';
export * from './validate';
export {
  getCountry, getSizeWindow, RedirectTo,
  UseComponentVisible, ScrollToTop,
  setNumberItem, useDocumentScrollThrottled,
  setHeightView, setShadow,
};
