import { useEffect, useRef, useState } from 'react';

import validateClose from '@/helpers/validateCloseMenu';

const UseComponentVisible = (initialIsVisible) => {
  const [isComponentVisible, setIsComponentVisible] = useState(
    initialIsVisible,
  );
  const ref = useRef(null);
  const onKeyPress = (event) => {
    if (event.keyCode === 40) {
      setIsComponentVisible(false);
    }
  };
  const onMouseDown = (event) => {
    if (validateClose(event.path) && ref.current && !ref.current.contains(event.target)) {
      setIsComponentVisible(false);
    }
  };

  useEffect(() => {
    document.addEventListener('keydown', onKeyPress);
    if (window.PointerEvent) {
      document.addEventListener('pointerdown', onMouseDown);
    } else {
      document.addEventListener('mousedown', onMouseDown);
    }
    return () => {
      document.removeEventListener('keydown', onKeyPress);
      if (window.PointerEvent) {
        document.removeEventListener('pointerdown', onMouseDown);
      } else {
        document.removeEventListener('mousedown', onMouseDown);
      }
    };
  });

  return {
    ref,
    isComponentVisible,
    setIsComponentVisible,
  };
};
export default UseComponentVisible;
