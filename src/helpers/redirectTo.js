const RedirectTo = (url) => {
  if (url !== '') {
    if (url.includes('http://') || url.includes('https://')) {
      window.location.assign(url);
    } else {
      window.location.assign(`http://${url}`);
    }
  }
};
export default RedirectTo;
