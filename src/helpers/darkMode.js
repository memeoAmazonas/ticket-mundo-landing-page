import colors from '../model/colors';

export const getDarkMode = () => {
  const theme = sessionStorage.getItem('theme') && sessionStorage.theme === 'dark' ? 'dark' : 'light';
  const color = theme === 'dark' ? colors.fourthColor : colors.charcoalGrey;
  return { color, theme };
};

export const getSkeletonColor = () => {
  const theme = sessionStorage.getItem('theme') && sessionStorage.theme === 'dark' ? 'dark' : 'light';
  return (theme === 'dark' ? colors.thirdColor : colors.ninthColor);
};
