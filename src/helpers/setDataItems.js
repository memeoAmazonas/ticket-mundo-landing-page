export const setDataMulticarousel = (sizeWindow) => {
  let itemWidth;
  let width;
  let height;
  let slidesPerPage;
  const minor = (sizeWindow <= 720);

  if (sizeWindow >= 1280 || minor) {
    if (sizeWindow === 1280) {
      itemWidth = 274;
    } else {
      itemWidth = minor ? 279 : 279;
    }
    width = 24.9;
    height = 36.8;
    slidesPerPage = minor ? 1 : 4;
  }

  if ((sizeWindow >= 1055 && sizeWindow < 1100) || (sizeWindow >= 768 && sizeWindow < 874)) {
    const size = sizeWindow >= 1055;
    slidesPerPage = size ? 4 : 3;
    const factor = size ? 0.12 : 0.1;
    itemWidth = Math.round((sizeWindow - sizeWindow * factor) / slidesPerPage);
    height = 31;
    width = 21;
  }
  if ((sizeWindow >= 1100 && sizeWindow < 1280)
    || (sizeWindow >= 900 && sizeWindow < 1055)
    || (sizeWindow >= 874 && sizeWindow < 968)
    || (sizeWindow > 720 && sizeWindow < 768)
  ) {
    const size = sizeWindow > 874;
    const factor = size ? 0 : 0.1;
    slidesPerPage = size ? 4 : 3;
    itemWidth = Math.round((sizeWindow - sizeWindow * 0.1) / slidesPerPage);
    width = Math.round((itemWidth * 89.24) / 100) * 0.1;
    height = width * 1.47;
    height -= height * factor;
  }

  return {
    arrows: !minor,
    centered: minor,
    itemWidth,
    height,
    slidesPerPage,
    width,
  };
};

export const setDataEvents = (sizeWindow) => {
  let width;
  let height;
  if (sizeWindow > 1200) {
    height = 23;
    width = 34.2;
  }
  if (sizeWindow >= 414 && sizeWindow <= 722) {
    height = 25;
    width = 37.4;
  }
  if ((sizeWindow >= 860 && sizeWindow <= 1200)
    || (sizeWindow > 722 && sizeWindow < 768)) {
    const itemWithPage = sizeWindow >= 800 ? 3 : 2;
    const factor = itemWithPage === 3 ? 0.087 : 0.03;
    width = ((sizeWindow - sizeWindow * 0.1) / itemWithPage);
    width -= width * factor;
    width *= 0.1;
    height = width * 0.672;
  }
  if (sizeWindow >= 768 && sizeWindow < 860) {
    width = 33;
    height = 22;
  }
  if (sizeWindow < 414) {
    width = Math.round((sizeWindow * 89.24) / 100) * 0.1;
    height = width * 0.672;
  }
  height += 'rem';
  width += 'rem';
  return { height, width };
};

// TODO cuando se arreglen los item volver a colocar como esta abajo
/* export const setWidthCategory = (sizeWindow, isLast) => {
  const factor = isLast ? 0.24 : 0.26;
  if (sizeWindow >= 1280 || sizeWindow <= 860) {
    if (sizeWindow > 722 && sizeWindow <= 729) {
      return 17;
    }
    if (sizeWindow <= 722) {
      return 18;
    }
    if (isLast) {
      return 10;
    }
    return 18;
  }

  return (sizeWindow * 0.56 * factor * 0.1);
}; */
export const setWidthCategory = (sizeWindow, isLast) => 18;

export const setWidthItemContactUs = (sizeWindow) => {
  if (sizeWindow > 768 && sizeWindow <= 1280) {
    return ((sizeWindow * 0.83) / 3);
  }
  if (sizeWindow > 414 < 768) {
    return 33;
  }
};
