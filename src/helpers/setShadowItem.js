import colors from '@/model/colors';

const setShadow = (theme) => {
  if (theme === 'light') {
    return {
      background: colors.fourthColor,
      boxShadow: '0 0 1rem 0 rgba(207, 216, 230,0.35)',
    };
  }
  return {
    background: colors.charcoalGrey,
    boxShadow: '0 0 1rem 0 rgba(0, 0, 0,0.20)',
  };
};
export default setShadow;
