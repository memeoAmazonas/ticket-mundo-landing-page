import cookie from 'react-cookies';

export const getCookie = (id) => cookie.load(id);
export const setCookie = (id, value, path = '/') => cookie.save(id, value, { path });
export const removeCookie = (id, path = '/') => cookie.remove(id, { path });
export const setSessionItem = (
  key, value,
) => window.sessionStorage.setItem(key, JSON.stringify(value));
export const getSessionItem = (key) => JSON.parse(window.sessionStorage.getItem(key));
export const clearAllData = () => window.sessionStorage.clear();
export const clearValue = (key) => window.sessionStorage.removeItem(key);
