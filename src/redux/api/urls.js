import config from '@/config';

const { idCountry, urlServices, itemsSearch, urlCdnImages } = config;
// INFO COUNTRY
export const LOCATION = 'https://api.ipdata.co/?api-key=a957070285289c62e4a57e00865606efb1e18ac8bfea9bfd70ada864';

// LIST CITY
export const CITY_LIST = `${urlServices}/evento/ciudades/${idCountry}`;

// HEADER CAROUSEL
export const ITEM_HEADER_CAROUSEL = `${urlServices}/landingPais/BannerPais/${idCountry}`;

// PRINCIPAL CAROUSEL VERTICAL
export const VERTICAL_CAROUSEL = `${urlServices}/landingPais/CarruselPais/${idCountry}`;

// IMAGES EVENTS
export const eventPoster = (eventoid) => `${urlCdnImages}/imagenestmusaprueba/Poster_Evento_${eventoid}_324_262.jpeg`;

// IMAGES EVENTS
export const eventPosterMulticarousel = (eventoid) => `${urlCdnImages}/imagenestmusaprueba/Evento_${eventoid}_360_182.jpeg`;

// CAROUSEL NEAR BY
export const eventNearBy = (latitude, longitude) => `${urlServices}/evento/distancia/${idCountry}/${latitude}/${longitude}`;

// EVENT BY CITY
export const eventByCity = (idCity) => `${urlServices}/evento/porciudad/${idCity}/${idCountry}`;

// SEARCH_EVENT
export const searchEvent = (content) => `${urlServices}/evento/ObtenerBuscador/${content}/${idCountry}/${itemsSearch}`;

// TKPLAY
export const TKPLAY = 'play.ticketmundo.com/';

// GET BROWSER NAVIGATOR
export const GET_BROWSER = 'https://api.duckduckgo.com/?q=useragent&format=json';

//GET ZIP IMAGE TICKETMUNDO
export const GET_BRAND_TICKETMUNDO = 'https://file-examples-com.github.io/uploads/2017/02/zip_2MB.zip';

//GET ZIP IMAGE PLAY
export const GET_BRAND_TICKETPLAY = 'https://file-examples-com.github.io/uploads/2017/02/zip_2MB.zip';

//EVENTS TICKETMUNDO PLAY
export const imagesTkmPlay = `${urlServices}/landingPais/BannerPlayPais/${idCountry}`
