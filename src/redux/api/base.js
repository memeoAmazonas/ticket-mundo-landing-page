import axios from 'axios';

const apiCall = (url, data, headers, method) => axios({
  method,
  url,
  data,
  headers,
});

export default apiCall;
