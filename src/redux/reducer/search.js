import {
  GET_CONTENT_SEARCH, GET_CONTENT_SEARCH_ERROR, GET_CONTENT_SEARCH_SUCCESS, RESET_SEARCH,
} from '../types';


export default (state = {}, { type, payload }) => {
  switch (type) {
    case GET_CONTENT_SEARCH:
      return {
        ...state,
        contentSearchLoading: true,
      };
    case GET_CONTENT_SEARCH_SUCCESS:
      return {
        ...state,
        contentSearchLoading: false,
        searchContent: payload,
      };
    case GET_CONTENT_SEARCH_ERROR:
      return {
        ...state,
        contentSearchLoading: false,
        searchContent: [],
      };
    case RESET_SEARCH:
      return {
        ...state,
        contentSearchLoading: false,
        searchContent: [],
      };
    default:
      return state;
  }
};
