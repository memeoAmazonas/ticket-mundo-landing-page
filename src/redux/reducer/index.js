import { combineReducers } from 'redux';

import search from '@/redux/reducer/search';
import event from './event';
import eventNearBy from './eventNearBy';
import header from './header';
import location from './country';
import utils from './utils';
import imagesTkPlay from './imagesTkPlay';

const reducer = combineReducers({
  event,
  eventNearBy,
  header,
  location,
  search,
  utils,
  imagesTkPlay,
});
export default reducer;
