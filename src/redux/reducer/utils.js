import {
  ACTUALLY_OPEN_ITEM_CONTACT_US,
  SET_COLOR_ICON_EVENT,
  SET_COLOR_ICON_HELP_ABOUTUS,
  SET_COLOR_ICON_SEARCH,
  SET_COOKIE,
  SET_FIND,
  SET_LANGUAGE,
  SET_LANGUAGE_VISIBLE,
  SET_LOADING_SKELETON,
  SET_MODAL_SELECT_COUNTRY_INIT, SET_NAME_CONTACT_US,
  SET_VISIBLE_CATEGORY_ABOUT_US,
} from '../types';

const INITIAL_STATE = {
  lang: '',
  nameCity: sessionStorage.nameCity ? sessionStorage.getItem('nameCity') : '',
  country: sessionStorage.country ? sessionStorage.getItem('country') : 0,
  isVisibleSearch: false,
  loadingSkeleton: true,
};
export default (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case SET_LANGUAGE:
      return { ...state, lang: payload };
    case SET_FIND:
      return { ...state, isVisibleSearch: payload };
    case SET_COOKIE:
      return { ...state, acceptCookie: payload };
    case SET_LOADING_SKELETON:
      return { ...state, loadingSkeleton: payload };
    case SET_VISIBLE_CATEGORY_ABOUT_US:
      return { ...state, visibleCategory: payload };
    case SET_MODAL_SELECT_COUNTRY_INIT:
      return { ...state, modalCountryInit: payload };
    case SET_LANGUAGE_VISIBLE:
      return { ...state, visibleLanguageList: payload };
    case SET_COLOR_ICON_EVENT:
      return { ...state, colorIconEvent: payload };
    case SET_COLOR_ICON_HELP_ABOUTUS:
      return { ...state, colorIconHelpAboutUs: payload };
    case SET_COLOR_ICON_SEARCH:
      return { ...state, colorIconSearch: payload };
    case SET_NAME_CONTACT_US:
      return { ...state, setNameContactUs: payload };
    case ACTUALLY_OPEN_ITEM_CONTACT_US:
      return { ...state, actuallyItemOpenContactUs: payload };
    default:
      return state;
  }
};
