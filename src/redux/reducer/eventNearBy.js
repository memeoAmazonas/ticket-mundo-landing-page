import {
  GET_EVENT_CAROUSEL, GET_EVENT_CAROUSEL_BY_CITY,
  GET_EVENT_CAROUSEL_ERROR,
  GET_EVENT_CAROUSEL_SUCCESS,
} from '@/redux/types';

export default (state = {}, { type, payload }) => {
  switch (type) {
    case GET_EVENT_CAROUSEL:
      return { ...state, nearByCarouselLoading: true };
    case GET_EVENT_CAROUSEL_BY_CITY:
      return { ...state, nearByCarouselLoading: true };
    case GET_EVENT_CAROUSEL_SUCCESS:
      return { ...state, nearByCarousel: payload, nearByCarouselLoading: false };
    case GET_EVENT_CAROUSEL_ERROR:
      return { ...state, nearByCarousel: [], nearByCarouselLoading: false };
    default:
      return { ...state };
  }
};
