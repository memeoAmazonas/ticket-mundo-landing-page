import {
  CHANGE_LOGO,
  GET_HEADER_LIST,
  GET_HEADER_LIST_ERROR,
  GET_HEADER_LIST_SUCCESS,
} from '@/redux/types';

export default (state = {}, { type, payload }) => {
  switch (type) {
    case CHANGE_LOGO:
      return { ...state, src: payload };
    case GET_HEADER_LIST:
      return { ...state, headerListLoading: true };
    case GET_HEADER_LIST_SUCCESS:
      return { ...state, headerListLoading: false, itemsHeader: payload };
    case GET_HEADER_LIST_ERROR:
      return { ...state, headerListLoading: false, itemsHeader: [] };
    default:
      return { ...state };
  }
};
