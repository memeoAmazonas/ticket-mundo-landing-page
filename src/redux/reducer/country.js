import {
  SET_ACTUALLY_CITY,
  SET_COUNTRY_INFO,
  SET_COUNTRY_INFO_SUCCESS,
  SET_COUNTRY_INFO_FAIL,
  SET_CITY_LIST,
  SET_CITY_LIST_SUCCESS,
  SET_CITY_LIST_FAIL,
} from '@/redux/types';

export default (state = {}, { type, payload }) => {
  switch (type) {
    case SET_COUNTRY_INFO:
      return { ...state, isLoading: true };
    case SET_COUNTRY_INFO_SUCCESS:
      return { ...state, isLoading: false, infoCountry: payload };
    case SET_COUNTRY_INFO_FAIL:
      return { ...state, isLoading: false, infoCountry: [] };
    case SET_CITY_LIST:
      return { ...state, isLoadingCityList: true };
    case SET_CITY_LIST_SUCCESS:
      return { ...state, isLoadingCityList: false, cityList: payload };
    case SET_CITY_LIST_FAIL:
      return { ...state, isLoadingCityList: false, cityList: [] };
    case SET_ACTUALLY_CITY:
      return { ...state, actuallyCity: payload };
    default:
      return { ...state };
  }
};
