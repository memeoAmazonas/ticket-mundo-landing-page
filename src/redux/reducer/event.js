import {
  GET_VERTICAL_CAROUSEL,
  GET_VERTICAL_CAROUSEL_ERROR,
  GET_VERTICAL_CAROUSEL_SUCCESS,
} from '@/redux/types';

export default (state = {}, { type, payload }) => {
  switch (type) {
    case GET_VERTICAL_CAROUSEL:
      return { ...state, verticalCarouselLoading: true };
    case GET_VERTICAL_CAROUSEL_SUCCESS:
      return { ...state, verticalCarousel: payload, verticalCarouselLoading: false };
    case GET_VERTICAL_CAROUSEL_ERROR:
      return { ...state, verticalCarousel: [], verticalCarouselLoading: false };
    default:
      return { ...state };
  }
};
