import { all } from 'redux-saga/effects';

import { getLocation, getCityList } from './country';
import { getVerticalCarousel } from './event';
import { getEventByCity } from './eventByCity';
import { getEventNearBy } from './eventNearBy';
import { getHeader } from './header';
import { getcontentSearch } from './search';
import { getImagesTKMPlay } from './imagesTkPlay';

export default function* rootSaga() {
  yield all([
    getCityList(),
    getHeader(),
    getLocation(),
    getVerticalCarousel(),
    getEventNearBy(),
    getEventByCity(),
    getcontentSearch(),
    getImagesTKMPlay(),
  ]);
}
