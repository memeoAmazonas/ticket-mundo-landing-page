import { put, call, takeLatest } from 'redux-saga/effects';
import {
  SET_COUNTRY_INFO,
  SET_COUNTRY_INFO_SUCCESS,
  SET_COUNTRY_INFO_FAIL,
  SET_CITY_LIST_FAIL, SET_CITY_LIST_SUCCESS, SET_CITY_LIST,
} from '@/redux/types';
import apiCall from '@/redux/api/base';
import { LOCATION, CITY_LIST, GET_BROWSER } from '@/redux/api/urls';
import config from '../../config';

export function* getInfoCountry() {
  try {
    const isBrave = yield call(apiCall, GET_BROWSER, null, null, 'GET');
    if (!isBrave.data.Answer.toUpperCase().includes('BRAVE')) {
      const payload = yield call(apiCall, LOCATION, null, null, 'GET');
      yield put({ type: SET_COUNTRY_INFO_SUCCESS, payload: payload.data });
    } else {
      const name = config.defaultLang === 'es' ? 'Spanish' : 'English';
      const brave = {
        latitude: 0,
        longitude: 0,
        country_code: config.countryCode,
        languages: [{ name }],
      };
      yield put({ type: SET_COUNTRY_INFO_SUCCESS, payload: brave });
    }
  } catch (error) {
    yield put({ type: SET_COUNTRY_INFO_FAIL, payload: error });
  }
}

export function* getLocation() {
  yield takeLatest(SET_COUNTRY_INFO, getInfoCountry);
}

export function* setCityList() {
  try {
    const response = yield call(apiCall, CITY_LIST, null, null, 'GET');
    yield put({
      type: SET_CITY_LIST_SUCCESS,
      payload: response.data,
    });
  } catch (error) {
    yield put({ SET_CITY_LIST_FAIL, payload: error });
  }
}

export function* getCityList() {
  yield takeLatest(SET_CITY_LIST, setCityList);
}
