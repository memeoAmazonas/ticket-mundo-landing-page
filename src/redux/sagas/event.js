import { call, put, takeLatest } from 'redux-saga/effects';
import {
  GET_VERTICAL_CAROUSEL,
  GET_VERTICAL_CAROUSEL_ERROR,
  GET_VERTICAL_CAROUSEL_SUCCESS,
} from '@/redux/types';
import apiCall from '@/redux/api/base';
import { VERTICAL_CAROUSEL } from '@/redux/api/urls';

export function* getItemCarouselVertical() {
  try {
    const payload = yield call(apiCall, VERTICAL_CAROUSEL, null, null, 'GET');
    yield put({ type: GET_VERTICAL_CAROUSEL_SUCCESS, payload: payload.data });
  } catch (error) {
    yield put({ type: GET_VERTICAL_CAROUSEL_ERROR, payload: error });
  }
}

export function* getVerticalCarousel() {
  yield takeLatest(GET_VERTICAL_CAROUSEL, getItemCarouselVertical);
}
