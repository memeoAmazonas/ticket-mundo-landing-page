import { put, call, takeLatest } from 'redux-saga/effects';

import config from '@/config';

import apiCall from '@/redux/api/base';
import { GET_HEADER_LIST, GET_HEADER_LIST_ERROR, GET_HEADER_LIST_SUCCESS } from '@/redux/types';
import { ITEM_HEADER_CAROUSEL } from '@/redux/api/urls';

const { urlFront } = config;

export function* getHeaderList() {
  try {
    const payload = yield call(apiCall, ITEM_HEADER_CAROUSEL, null, { 'Access-Control-Allow-Origin': urlFront, 'Content-Type': 'application/json' }, 'GET');
    yield put({ type: GET_HEADER_LIST_SUCCESS, payload: payload.data });
  } catch (error) {
    yield put({ type: GET_HEADER_LIST_ERROR, payload: error });
  }
}
export function* getHeader() {
  yield takeLatest(GET_HEADER_LIST, getHeaderList);
}
