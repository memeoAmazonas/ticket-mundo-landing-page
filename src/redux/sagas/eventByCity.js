import { call, put, takeLatest } from 'redux-saga/effects';
import {
  GET_EVENT_CAROUSEL_BY_CITY,
  GET_EVENT_CAROUSEL_ERROR,
  GET_EVENT_CAROUSEL_SUCCESS,
} from '@/redux/types';
import apiCall from '@/redux/api/base';
import { eventByCity } from '@/redux/api/urls';

export function* getItemCarouselByCity({ payload }) {
  const { idCity } = payload;
  const url = eventByCity(idCity);
  try {
    const response = yield call(apiCall, url, null, null, 'GET');
    yield put({ type: GET_EVENT_CAROUSEL_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: GET_EVENT_CAROUSEL_ERROR, payload: error });
  }
}

export function* getEventByCity() {
  yield takeLatest(GET_EVENT_CAROUSEL_BY_CITY, getItemCarouselByCity);
}
