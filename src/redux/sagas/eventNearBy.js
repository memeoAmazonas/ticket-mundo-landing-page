import { call, put, takeLatest } from 'redux-saga/effects';
import {
  GET_EVENT_CAROUSEL,
  GET_EVENT_CAROUSEL_ERROR,
  GET_EVENT_CAROUSEL_SUCCESS,
} from '@/redux/types';
import apiCall from '@/redux/api/base';
import { eventNearBy } from '@/redux/api/urls';


export function* getItemCarouselNearBy({ payload }) {
  const { latitude, longitude } = payload;
  const url = eventNearBy(latitude, longitude);
  try {
    const response = yield call(apiCall, url, null, null, 'GET');
    yield put({ type: GET_EVENT_CAROUSEL_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: GET_EVENT_CAROUSEL_ERROR, payload: error });
  }
}

export function* getEventNearBy() {
  yield takeLatest(GET_EVENT_CAROUSEL, getItemCarouselNearBy);
}
