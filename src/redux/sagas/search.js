import { call, put, takeLatest } from 'redux-saga/effects';
import {
  GET_CONTENT_SEARCH, GET_CONTENT_SEARCH_ERROR, GET_CONTENT_SEARCH_SUCCESS,
} from '@/redux/types';
import apiCall from '@/redux/api/base';
import { searchEvent } from '@/redux/api/urls';

export function* getItemContentSearch({ payload }) {
  const { content } = payload;
  const url = searchEvent(content);
  try {
    const response = yield call(apiCall, url, null, null, 'GET');
    yield put({ type: GET_CONTENT_SEARCH_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: GET_CONTENT_SEARCH_ERROR, payload: error });
  }
}

export function* getcontentSearch() {
  yield takeLatest(GET_CONTENT_SEARCH, getItemContentSearch);
}
