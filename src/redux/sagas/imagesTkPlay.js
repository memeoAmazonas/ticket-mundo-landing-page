import { call, put, takeLatest } from 'redux-saga/effects';

import apiCall from '@/redux/api/base';
import { GET_IMAGES_TKMPLAY, GET_IMAGES_TKMPLAY_ERROR, GET_IMAGES_TKMPLAY_SUCCESS } from '../types';
import { imagesTkmPlay } from '../api/urls';


export function* getImagesTkmPlay() {
  try {
    const response = yield call(apiCall, imagesTkmPlay, null, null, 'GET');
    yield put({ type: GET_IMAGES_TKMPLAY_SUCCESS, payload: response.data });
  } catch (error) {
    yield put({ type: GET_IMAGES_TKMPLAY_ERROR, payload: error });
  }
}

export function* getImagesTKMPlay() {
  yield takeLatest(GET_IMAGES_TKMPLAY, getImagesTkmPlay);
}
