import {
  GET_EVENT_CAROUSEL,
  GET_EVENT_CAROUSEL_BY_CITY,
  GET_VERTICAL_CAROUSEL,
} from '@/redux/types';

export const GetVerticalCarousel = () => ({ type: GET_VERTICAL_CAROUSEL });
export const GetEventNearBy = (payload) => ({ type: GET_EVENT_CAROUSEL, payload });
export const GetEventByCity = (payload) => ({ type: GET_EVENT_CAROUSEL_BY_CITY, payload });
