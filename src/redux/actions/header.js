import { CHANGE_LOGO, GET_HEADER_LIST } from '../types';

export const ChangeLogo = (payload) => ({ type: CHANGE_LOGO, payload });
export const GetHeader = () => ({ type: GET_HEADER_LIST });
