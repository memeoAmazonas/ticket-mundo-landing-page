import { setCookie } from '@/helpers/setCookies';
import {
  GET_CONTENT_SEARCH,
  SET_MODAL_SELECT_COUNTRY_INIT,
  SET_LANGUAGE,
  SET_FIND,
  SET_COOKIE,
  SET_LOADING_SKELETON,
  SET_VISIBLE_CATEGORY_ABOUT_US,
  RESET_SEARCH,
  SET_LANGUAGE_VISIBLE,
} from '@/redux/types';

export const SetLocale = (lang) => {
  setCookie('language', lang);
  return {
    type: SET_LANGUAGE,
    payload: lang,
  };
};

export const SetFind = (payload) => ({ type: SET_FIND, payload });

export const SetCookie = (payload) => ({ type: SET_COOKIE, payload });

export const SetLoadingSkeleton = (payload) => ({ type: SET_LOADING_SKELETON, payload });

export const SetVisibleCategory = (payload) => ({ type: SET_VISIBLE_CATEGORY_ABOUT_US, payload });
export const SetVisibleModalInitCountrySelected = (payload) => ({
  type: SET_MODAL_SELECT_COUNTRY_INIT,
  payload,
});
export const GetcontentSearch = (payload) => ({
  type: GET_CONTENT_SEARCH,
  payload,
});
export const ResetSearch = () => ({ type: RESET_SEARCH });
export const SetLanguageVisibleAboutUs = (payload) => ({
  type: SET_LANGUAGE_VISIBLE,
  payload,
});
export const SetColor = (type, payload) => ({
  type,
  payload,
});

export const setAction = (type, payload = null) => ({ type, payload });
