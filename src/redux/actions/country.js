import { SET_ACTUALLY_CITY, SET_CITY_LIST, SET_COUNTRY_INFO } from '../types';

export const SetInfoCountry = () => ({ type: SET_COUNTRY_INFO });
export const GetListCity = () => ({ type: SET_CITY_LIST });
export const SetActuallyCity = (payload) => ({ type: SET_ACTUALLY_CITY, payload });
