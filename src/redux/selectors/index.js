import { get } from 'lodash';
import { useSelector } from 'react-redux';

const getSelector = (store, key) => get(store, key);
export default getSelector;

export function getRedux(key) {
  return useSelector((state) => getSelector(state, key));
}
