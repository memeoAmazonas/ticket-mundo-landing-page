import React from 'react';
import { uniqueId } from 'lodash';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import { getDarkMode } from '@/helpers';
import { colors } from '@/model';
import zip from '@/static/images/brand-resources/zip-icon.svg';
import download from '@/static/images/brand-resources/download-icon.svg';

const ItemBrandResource = ({
  name, size, src, downloadLink,
}) => {
  const { theme } = getDarkMode();
  const backgroundColor = theme === 'light' ? colors.twentieOnethColor : colors.seventhColor;
  return (
    <div className="item-brand-resource-container">
      <div className="item-brand-resource-container__image" style={{ backgroundImage: `url(${src})` }} />
      <a
        href={downloadLink}
        target="_blank"
        rel="noopener noreferrer"
        download
        className="item-brand-resource-container__content"
        style={{ backgroundColor }}
      >
        <>
          <div className="item-brand-resource-container__content-image">
            <img alt={`brand-resource-${uniqueId()}`} src={zip} />
            <div className="item-brand-resource-container__content-image__text">
              <span className="item-brand-resource-container__content-image__text-title">
                <FormattedMessage
                  id={name}
                />
              </span>
              <span className="item-brand-resource-container__content-image__text-subtitle">
                {size}
                MB
              </span>
            </div>
          </div>
          <img
            src={download}
            className="item-brand-resource-container__content__download"
            alt={`download-${uniqueId()}`}
          />
        </>
      </a>
    </div>
  );
};

ItemBrandResource.propTypes = {
  name: PropTypes.string.isRequired,
  size: PropTypes.string.isRequired,
  src: PropTypes.string.isRequired,
  downloadLink: PropTypes.string.isRequired,
};

export default ItemBrandResource;
