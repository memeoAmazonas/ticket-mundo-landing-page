import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import Button from '@/components/button/button';
import { getDarkMode } from '@/helpers/darkMode';
import { colors } from '@/model';

const ItemMenuTitle = ({
  className, classNameButton, title, width,
}) => {
  const { theme } = getDarkMode();
  let dark;
  let light;
  const isTheme = theme === 'light';
  if (isTheme) {
    if (className !== 'content-menu-container__item') {
      light = colors.blueyGrey;
    } else {
      light = colors.charcoalGrey;
    }
  } else if (className !== 'content-menu-container__item') {
    dark = colors.fourthColor;
  } else {
    dark = colors.firstColor;
  }
  return (
    <div className={className}>
      <Button
        className={classNameButton}
        content={<FormattedMessage id={title} />}
        dark={dark}
        light={light}
        width={width}
      />
    </div>
  );
};

ItemMenuTitle.defaultProps = {
  className: 'content-menu-container__item',
  classNameButton: 'button-principal__item-menu',
  title: '',
  width: 0,
};

ItemMenuTitle.propTypes = {
  className: PropTypes.string,
  classNameButton: PropTypes.string,
  title: PropTypes.string,
  width: PropTypes.number,
};

export default ItemMenuTitle;
