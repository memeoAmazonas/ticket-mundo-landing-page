import React from 'react';
import PropTypes from 'prop-types';
import { Image } from 'semantic-ui-react';
import { FormattedMessage } from 'react-intl';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import Button from '@/components/button/button';
import { colors } from '@/model';

const ItemCountry = ({
  icon, name, onClick, src, visible,
}) => {
  const styles = {
    color: colors.blueyGrey,
    display: visible ? 'inline-block' : 'none',
    margin: '3% 8.9% 0 0',
    float: 'right',
  };
  const content = (
    <div className="item-country-container__content">
      <Image alt="flag" src={src} className="item-country-container__content__image" />
      <span className="item-country-container__content__label"><FormattedMessage id={name} /></span>
      {icon && <FontAwesomeIcon icon={icon} style={styles} />}
    </div>
  );
  return (
    <Button content={content} className="item-country-container" onClick={onClick} />
  );
};

ItemCountry.propTypes = {
  icon: PropTypes.object,
  name: PropTypes.string,
  onClick: PropTypes.func,
  src: PropTypes.string,
  visible: PropTypes.bool,
};

ItemCountry.defaultProps = {
  icon: null,
  name: '',
  onClick: null,
  src: '',
  visible: false,
};

export default ItemCountry;
