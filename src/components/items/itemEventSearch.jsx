import React from 'react';
import { uniqueId } from 'lodash';
import PropTypes from 'prop-types';
import { Image } from 'semantic-ui-react';

import colors from '@/model/colors';

import {
  getSizeWindow, getDarkMode, setShadow, RedirectTo, setDataEvents,
} from '@/helpers';

const ItemEventSearch = ({
  city, name, pattern, src, url,
}) => {
  const { theme } = getDarkMode();
  const setLabel = () => {
    const actually = name.toLocaleLowerCase()
      .split(pattern.toLocaleLowerCase());
    return actually.map((item, index) => {
      if (item === '' && index !== actually.length - 1) {
        return <strong key={uniqueId()}>{pattern.toLocaleLowerCase()}</strong>;
      }
      if (index !== actually.length - 1) {
        return (
          <span key={uniqueId()}>
            {item.toLocaleLowerCase()}
            <strong>{pattern.toLocaleLowerCase()}</strong>
          </span>
        );
      }
      return (
        <span key={uniqueId()}>
          {item.toLocaleLowerCase()}
        </span>
      );
    });
  };
  let colorName;
  let colorCity;
  if (theme === 'light') {
    colorName = colors.charcoalGrey;
    colorCity = colors.blueyGrey;
  } else {
    colorName = colors.fourthColor;
    colorCity = colors.fourthColor;
  }
  const st = { ...setDataEvents(getSizeWindow().width), ...setShadow(theme) };
  return (
    <div className="card-item-event-search-container" style={st}>
      <Image
        src={src}
        className="card-item-event-search-container__image"
        onClick={() => RedirectTo(url)}
      />
      <div className="card-item-event-search-container__legend">
        <div className="card-item-event-search-container__legend__name" style={{ color: colorName }}>
          {setLabel()}
        </div>
        <div className="card-item-event-search-container__legend__city" style={{ color: colorCity }}>
          {city}
        </div>
      </div>
    </div>
  );
};

ItemEventSearch.propTypes = {
  city: PropTypes.string,
  name: PropTypes.string,
  pattern: PropTypes.string,
  src: PropTypes.string,
  url: PropTypes.string,
};
ItemEventSearch.defaultProps = {
  city: '',
  name: '',
  pattern: '',
  src: '',
  url: '',
};

export default ItemEventSearch;
