import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import Button from '@/components/button/button';
import Image from '@/components/image/image';
import { getDarkMode } from '@/helpers';
import colors from '@/model/colors';

const ItemCountryModal = ({
  actually, name, nameUrl, onClick, src, width,
}) => {
  const { theme } = getDarkMode();
  const color = theme === 'dark' ? colors.firstColor : colors.thirdColor;
  return (
    <div className="country-container">
      <div className="country-container__flag-label">
        <Image height={2} src={src} width={3} />
        <span className="country-container__flag-label__label" style={{ color }}>
          <FormattedMessage id={name} />
        </span>
      </div>
      <div className="country-container__flag-label__button">
        <Button
          className="button-principal__country"
          content={actually ? <FormattedMessage id={nameUrl} /> : nameUrl}
          onClick={onClick}
          width={width}
        />
      </div>
    </div>
  );
};
ItemCountryModal.defaultProps = {
  actually: false,
  name: null,
  nameUrl: null,
  onClick: null,
  src: null,
  width: 0,
};

ItemCountryModal.propTypes = {
  actually: PropTypes.bool,
  name: PropTypes.string,
  nameUrl: PropTypes.any,
  onClick: PropTypes.func,
  src: PropTypes.string,
  width: PropTypes.number,
};

export default ItemCountryModal;
