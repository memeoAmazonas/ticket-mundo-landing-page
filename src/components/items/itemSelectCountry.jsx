import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import Button from '../button/button';

const ItemSelectCountry = ({
  link, src, name, onClick, theme,
}) => (
  <div className={`item-select-country-container item-shadow-${theme}`}>
    <Button
      className="button-principal__item-select-country"
      onClick={onClick}
      content={(
        <div className="button-principal__item-select-country__content">
          <img src={src} alt={name} />
          <div className="button-principal__item-select-country__content-legend">
            <div className="button-principal__item-select-country__content-legend-country">
              <FormattedMessage id={name} />
            </div>
            <div className="button-principal__item-select-country__content-legend-url">
              {link}
            </div>
          </div>
        </div>
      )}
    />
  </div>
);
ItemSelectCountry.propTypes = {
  link: PropTypes.string.isRequired,
  theme: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  src: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default ItemSelectCountry;
