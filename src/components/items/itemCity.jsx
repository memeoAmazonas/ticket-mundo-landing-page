import React from 'react';
import PropTypes from 'prop-types';

import RadioButton from '@/components/button/radioButton';
import { getDarkMode } from '@/helpers';
import { getRedux } from '@/redux/selectors';
import { keyLocationCityList } from '@/redux/selectors/selectorsKey';

const ItemCity = ({
  checked, name, onChange, value,
}) => {
  const { theme } = getDarkMode();
  const cityList = getRedux(keyLocationCityList);
  let classname;
  if (cityList) {
    if (cityList.length > 8) {
      classname = `item-city-container__border-bottom__${theme}`;
    } else {
      classname = `item-city-container__normal__${theme}`;
    }
  }
  return (
    <RadioButton
      classname={classname}
      checked={checked}
      name={name}
      onChange={onChange}
      value={value}
    />
  );
};

ItemCity.defaultProps = {
  checked: false,
  name: '',
  onChange: null,
  value: null,
};

ItemCity.propTypes = {
  checked: PropTypes.bool,
  name: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.number,
};

export default ItemCity;
