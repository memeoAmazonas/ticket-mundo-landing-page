import React from 'react';
import { uniqueId } from 'lodash';
import PropTypes from 'prop-types';
import { Image } from 'react-bootstrap';
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';

import { getDarkMode } from '@/helpers';
import { colors } from '@/model';

const ItemContactUs = ({
  content, title, to, src,
}) => {
  const { theme } = getDarkMode();

  const colorTitle = theme === 'light' ? colors.charcoalGrey : colors.fourthColor;
  const visibilityImage = src ? 'visible' : 'hidden';
  const visibilityTitle = title ? 'visible' : 'hidden';
  const visibilityContent = content ? 'visible' : 'hidden';

  return (
    <Link to={`/contact-us/detail/${to}`} className={`item-contactus-container item-shadow-swu-${theme}`}>
      <Image src={src} className="item-contactus-container__image" style={{ visibility: visibilityImage }} alt={`${uniqueId()}-contact-us`} />
      <div className="item-contactus-container__content">
        <span
          className="item-contactus-container__content-title"
          style={{ color: colorTitle, visibility: visibilityTitle }}
        >
          <FormattedMessage id={title} />
        </span>
        <p
          className="item-contactus-container__content-content"
          style={{ visibility: visibilityContent }}
        >
          <FormattedMessage id={content} />
        </p>
      </div>
    </Link>
  );
};

ItemContactUs.propTypes = {
  content: PropTypes.string.isRequired,
  to: PropTypes.string.isRequired,
  src: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
};

export default ItemContactUs;
