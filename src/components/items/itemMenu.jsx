import React from 'react';
import PropTypes from 'prop-types';

const ItemMenu = ({ content, theme }) => (
  <div className={`content-menu-container__item content-menu-container__item-${theme}`}>
    {content}
  </div>
);

ItemMenu.defaultProps = {
  content: null,
  theme: '',
};

ItemMenu.propTypes = {
  content: PropTypes.element,
  theme: PropTypes.string,
};

export default ItemMenu;
