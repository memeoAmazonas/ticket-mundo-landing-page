import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import { getDarkMode } from '@/helpers';
import { colors } from '@/model';

const ItemSellWithUsCategory = ({ src, label, onClick }) => {
  const { theme } = getDarkMode();
  const color = theme === 'light' ? colors.charcoalGrey : colors.fourthColor;

  return (
    <div className={`item-sell-wuc-container item-shadow-swu-${theme}`} onClick={onClick}>
      <div style={{ backgroundImage: `url(${src})` }} className="item-sell-wuc-container__image" />
      <span className="item-sell-wuc-container__label" style={{ color }}><FormattedMessage id={label} /></span>
    </div>
  );
};
ItemSellWithUsCategory.propTypes = {
  src: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func,
};
ItemSellWithUsCategory.defaultProps = {
  onClick: null,
};
export default ItemSellWithUsCategory;
