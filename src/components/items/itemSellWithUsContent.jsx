import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import { getDarkMode } from '@/helpers';

const ItemSellWithUsContent = ({
  color, content, src, title, type,
}) => {
  const { theme } = getDarkMode();
  return (
    <div className={`item-sell-wus-content-container-${type} item-shadow-swu-${theme}`}>
      <div className={`item-sell-wus-content-container-${type}__image`} style={{ backgroundImage: `url(${src})` }} />
      <div className={`item-sell-wus-content-container-${type}__content`}>
        <div className={`item-sell-wus-content-container-${type}__content-title`} style={{ color }}>
          <FormattedMessage id={title} />
        </div>
        <div className={`item-sell-wus-content-container-${type}__content-legend`}>
          <FormattedMessage id={content} />
        </div>
      </div>
    </div>
  );
};
ItemSellWithUsContent.propTypes = {
  color: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  src: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
};

export default ItemSellWithUsContent;
