import React from 'react';
import PropTypes from 'prop-types';
import { Image } from 'semantic-ui-react';

import { getDarkMode, setShadow } from '@/helpers';

const MultiCarouselItem = ({
  height, location, marginLeft, marginRight, onclick, onTouchEnd, onTouchstart, src, title, width,
}) => {
  const { theme } = getDarkMode();
  const styles = {
    height: `${height}rem`,
    marginLeft: `${marginLeft}rem`,
    marginRight: `${marginRight}rem`,
    width: `${width}rem`,
    ...setShadow(theme),
  };
  return (
    <div
      className="multi-carousel-item-container"
      style={styles}
      onTouchStart={onTouchstart}
      onTouchEnd={onTouchEnd}
    >
      <Image alt="image carousel" src={src} className="multi-carousel-item-container__image" onClick={onclick} />
      <div className="multi-carousel-item-container__title">{title}</div>
      <div className="multi-carousel-item-container__location">{location}</div>
    </div>
  );
};
MultiCarouselItem.defaultProps = {
  height: 0,
  location: '',
  marginLeft: 0,
  marginRight: 0,
  onclick: null,
  onTouchEnd: null,
  onTouchstart: null,
  src: '',
  title: '',
  width: 0,
};
MultiCarouselItem.propTypes = {
  height: PropTypes.number,
  location: PropTypes.string,
  marginLeft: PropTypes.number,
  marginRight: PropTypes.number,
  onclick: PropTypes.func,
  onTouchEnd: PropTypes.func,
  onTouchstart: PropTypes.func,
  src: PropTypes.string,
  title: PropTypes.string,
  width: PropTypes.number,
};
export default MultiCarouselItem;
