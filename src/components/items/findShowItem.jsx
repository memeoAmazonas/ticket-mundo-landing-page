import React from 'react';
import PropTypes from 'prop-types';
import { Image } from 'semantic-ui-react';
import { FormattedMessage } from 'react-intl';

import Button from '@/components/button/button';
import { getSizeWindow, setDataEvents } from '@/helpers';

const FindShowItem = ({ onClick, src }) => {
  const styles = setDataEvents(getSizeWindow().width);
  return (
    <div className="find-show-container-item">
      <div className="find-show-container-item__image-content" style={styles}>
        <Image
          alt="image search"
          className="find-show-container-item__image-content__image"
          src={src}
        />
        <Button
          className="button-principal__find-show-item"
          content={<FormattedMessage id="requestShow" />}
          onClick={onClick}
        />
      </div>
    </div>
  );
};
FindShowItem.propTypes = {
  onClick: PropTypes.func,
  src: PropTypes.string,
};
FindShowItem.defaultProps = {
  onClick: null,
  src: '',
};

export default FindShowItem;
