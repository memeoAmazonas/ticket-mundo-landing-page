import React from 'react';
import PropTypes from 'prop-types';
import { Image } from 'semantic-ui-react';
import { FormattedMessage } from 'react-intl';

import colors from '@/model/colors';
import { getDarkMode, getSizeWindow, setDataEvents } from '@/helpers';

const ItemCarEvent = ({
  day, month, name, onclick, src, srcIcon, ubication, iconLabel,
}) => {
  const setDate = () => {
    if (day < 10) {
      return `0${day}`;
    }
    return day;
  };
  const { theme } = getDarkMode();
  const color = theme === 'light' ? colors.black : colors.fourthColor;

  return (
    <div
      aria-hidden="true"
      className={`card-item-event-container item-shadow-${theme}`}
      onClick={onclick}
      style={{ ...setDataEvents(getSizeWindow().width) }}
    >
      <Image
        alt="Card item event image"
        className="card-item-event-container__image"
        onClick={onclick}
        src={src}
      />
      <div className="card-item-event-container__legend">
        <div className="card-item-event-container__legend__name-ubication col-11">
          <span className="card-item-event-container__legend__name-ubication__name" style={{ color }}>
            {name}
          </span>
          <span className="card-item-event-container__legend__name-ubication__ubication">
            {ubication}
          </span>
        </div>
        <div className="card-item-event-container__legend__date">
          <div className="card-item-event-container__legend__date__day">{setDate()}</div>
          <div className="card-item-event-container__legend__date__month">
            <FormattedMessage id={`month.${month}`} />
          </div>
        </div>
      </div>
      {srcIcon
      && (
      <div className="card-item-event-container__icon">
        <Image src={srcIcon} className="card-item-event-container__icon-image" />
        <span className="card-item-event-container__icon-label"><FormattedMessage id={iconLabel} /></span>
      </div>
      )}

    </div>
  );
};

ItemCarEvent.defaultProps = {
  day: null,
  month: null,
  name: '',
  onclick: null,
  src: '',
  srcIcon: null,
  ubication: '',
  iconLabel: '',
};

ItemCarEvent.propTypes = {
  day: PropTypes.number,
  month: PropTypes.number,
  name: PropTypes.string,
  onclick: PropTypes.func,
  src: PropTypes.string,
  srcIcon: PropTypes.string,
  ubication: PropTypes.string,
  iconLabel: PropTypes.string,
};

export default ItemCarEvent;
