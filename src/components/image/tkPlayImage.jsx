import React from 'react';
import PropTypes from 'prop-types';
import { Image } from 'semantic-ui-react';

import Button from '@/components/button/button';
import { getSizeWindow, RedirectTo, setDataMulticarousel } from '@/helpers';
import { TKPLAY } from '@/redux/api/urls';
import iconSvg from '@/static/images/icon-play.svg';

const TkPlayImage = ({ display, src, url }) => {
  const { width, height } = setDataMulticarousel(getSizeWindow().width);
  const styles = {
    display: display ? 'inline-block' : 'none',
    height: `${height}rem`,
    width: `${width}rem`,
  };
  return (
    <div className="tk-play-image-container" style={styles} onClick={() => RedirectTo(url)}>
      <Image alt="ticketmundo play image" src={src} className="tk-play-image-container__image" />
      <Button
        className="button-principal__tkplay-image"
        style={{ backgroundImage: `url(${iconSvg})` }}
        onClick={() => RedirectTo(url)}
      />
    </div>
  );
};

TkPlayImage.propTypes = {
  display: PropTypes.bool,
  src: PropTypes.string,
  url: PropTypes.string,
};

TkPlayImage.defaultProops = {
  display: false,
  src: '',
  url: TKPLAY,
};
export default TkPlayImage;
