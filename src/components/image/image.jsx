import React from 'react';
import PropTypes from 'prop-types';

const Image = ({
  alt, height, marginBottom, marginLeft, marginRight, marginTop, src, width, onClick,
}) => {
  const styles = {
    height: `${height}rem`,
    marginBottom: `${marginBottom}rem`,
    marginLeft: `${marginLeft}rem`,
    marginRight: `${marginRight}rem`,
    marginTop: `${marginTop}rem`,
    width: `${width}rem`,
  };
  return (
    <img alt={alt} src={src} style={{ ...styles, cursor: 'pointer' }} onClick={onClick} />
  );
};

Image.defaultProps = {
  alt: null,
  height: 1.6,
  marginBottom: 0,
  marginLeft: 0,
  marginRight: 0,
  marginTop: 0,
  onClick: null,
  src: null,
  width: 15.2,
};
Image.propTypes = {
  alt: PropTypes.string,
  height: PropTypes.number,
  marginBottom: PropTypes.number,
  marginLeft: PropTypes.number,
  marginRight: PropTypes.number,
  marginTop: PropTypes.number,
  onClick: PropTypes.func,
  src: PropTypes.string,
  width: PropTypes.number,
};
export default Image;
