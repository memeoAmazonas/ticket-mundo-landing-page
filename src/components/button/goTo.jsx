import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Image from 'react-bootstrap/Image';

import { getDarkMode } from '@/helpers';
import dark from '@/static/images/back-icon-dark.svg';
import light from '@/static/images/back-icon-light.svg';

const GoTo = ({ to }) => {
  const { theme } = getDarkMode();
  const icon = theme === 'light' ? light : dark;
  return (
    <Link to={to}>
      <Image src={icon} className="goto-container" />
    </Link>
  );
};

GoTo.propTypes = { to: PropTypes.string.isRequired };
export default GoTo;
