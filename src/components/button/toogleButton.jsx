import React from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { useAccordionToggle } from 'react-bootstrap/AccordionToggle';

import { setAction } from '@/redux/actions';
import { getRedux } from '@/redux/selectors';
import { keyActuallyItemOpenContactUs } from '@/redux/selectors/selectorsKey';
import { ACTUALLY_OPEN_ITEM_CONTACT_US } from '@/redux/types';

const ToogleButton = ({ children, eventKey }) => {
  const dispatch = useDispatch();
  const isOpen = getRedux(keyActuallyItemOpenContactUs);
  const decoratedOnClick = useAccordionToggle(eventKey,
    () => dispatch(setAction(
      ACTUALLY_OPEN_ITEM_CONTACT_US, eventKey === isOpen ? null : eventKey,
    )));

  return (
    <button
      type="button"
      className="list-accordion-content__header"
      onClick={decoratedOnClick}
    >
      {children}
    </button>
  );
};
ToogleButton.propTypes = {
  children: PropTypes.element.isRequired,
  eventKey: PropTypes.number.isRequired,
};

export default ToogleButton;
