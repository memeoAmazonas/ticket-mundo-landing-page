import React from 'react';
import { uniqueId } from 'lodash';
import PropTypes from 'prop-types';

import { getDarkMode } from '@/helpers';
import colors from '@/model/colors';

const Button = ({
  className, content, dark, disabled, id, light,
  height, onAnimationEnd, onClick, onMouseOver, onMouseOut, style, width,
}) => {
  const { theme } = getDarkMode();
  const validate = theme === 'dark';
  const styles = {
    height: `${height}rem`,
    color: validate ? dark : light,
    width: `${width}rem`,
    ...style,
  };
  return (
    <button
      aria-label="justify"
      className={className}
      id={id}
      onAnimationEnd={onAnimationEnd}
      onClick={onClick}
      onMouseOver={onMouseOver}
      onMouseOut={onMouseOut}
      style={styles}
      type="button"
      name={`btn-${uniqueId()}`}
      disabled={disabled}
    >
      {content}
    </button>
  );
};
Button.defaultProps = {
  className: 'button-principal',
  content: null,
  dark: colors.fourthColor,
  disabled: false,
  id: uniqueId(),
  height: 1.6,
  light: colors.charcoalGrey,
  onAnimationEnd: null,
  onClick: null,
  onMouseOut: null,
  onMouseOver: null,
  style: {},
  width: 15.2,
};
Button.propTypes = {
  className: PropTypes.string,
  content: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.element]),
  dark: PropTypes.string,
  disabled: PropTypes.bool,
  id: PropTypes.string,
  height: PropTypes.number,
  light: PropTypes.string,
  onAnimationEnd: PropTypes.func,
  onClick: PropTypes.func,
  onMouseOut: PropTypes.func,
  onMouseOver: PropTypes.func,
  style: PropTypes.shape({}),
  width: PropTypes.number,
};
export default Button;
