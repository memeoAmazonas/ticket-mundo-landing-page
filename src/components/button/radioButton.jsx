import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import { getDarkMode } from '@/helpers';
import colors from '@/model/colors';

const RadioButton = ({
  classname, checked, name, onChange, value, isLabel,
}) => {
  const label = isLabel ? <FormattedMessage id={name} /> : name;
  const { theme } = getDarkMode();
  const color = theme === 'dark' ? colors.fourthColor : colors.charcoalGrey;

  return (
    <div className={classname} onClick={onChange}>
      <div className="radio-button-container">
        <label htmlFor={name} className="radio-button-container__name" style={{ color }}>
          { label }
        </label>
        <div className="radio-button-container__radio" onClick={onChange}>
          <button
            className="radio-button-container__radio__content"
            onClick={onChange}
            type="button"
          >
            <input
              className="radio-button-container__radio__content__input"
              checked={checked}
              id={name}
              onChange={onChange}
              type="radio"
              value={value}
            />
            <span
              className="radio-button-container__radio__content__content"
              style={{ display: checked ? 'block' : 'none' }}
            />
          </button>
        </div>
      </div>
    </div>
  );
};

RadioButton.defaultProps = {
  checked: false,
  classname: '',
  isLabel: false,
  name: '',
  onChange: null,
};

RadioButton.propTypes = {
  checked: PropTypes.bool,
  classname: PropTypes.string,
  isLabel: PropTypes.bool,
  name: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
};

export default RadioButton;
