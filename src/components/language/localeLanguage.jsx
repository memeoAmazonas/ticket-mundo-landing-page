import { filter } from 'lodash';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { useDispatch } from 'react-redux';
import { faAngleDown } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import config from '@/config';
import Button from '@/components/button/button';
import { getDarkMode } from '@/helpers';
import { SetLocale } from '@/redux/actions';
import { getRedux } from '@/redux/selectors';
import { keyLanguage } from '@/redux/selectors/selectorsKey';

const LocaleLanguage = () => {
  const { languageList } = config;
  const dispatch = useDispatch();
  const language = getRedux(keyLanguage);
  const { theme } = getDarkMode();
  const [visible, setVisible] = React.useState(false);
  const items = languageList.map((item) => (
    (item.lang !== language)
      ? (
        <Button
          className={`button-principal__language-list button-principal__language-list__${theme}`}
          content={(
            <FormattedMessage
              id={item.id}
            />
          )}
          id={item.lang}
          height={4.5}
          key={item.lang}
          onClick={() => { dispatch(SetLocale(item.lang)); setVisible(false); }}
          width={9.5}
        />
      ) : null
  ));
  const actually = filter(languageList, (item) => item.lang === language);
  const contentButton = (
    <div className="language-container__content-button">
      <span className="language-container__content-button__label"><FormattedMessage id={actually[0].id} /></span>
      <FontAwesomeIcon
        icon={faAngleDown}
      />
    </div>
  );
  return (
    <div id="set-language" className="language-container">
      <Button
        id="set-language-button"
        className="button-principal__set-language"
        content={contentButton}
        height={2.5}
        onClick={() => setVisible(!visible)}
        width={11.9}
      />
      <div style={{ display: visible ? 'block' : 'none' }}>
        {items}
      </div>
    </div>
  );
};

export default LocaleLanguage;
