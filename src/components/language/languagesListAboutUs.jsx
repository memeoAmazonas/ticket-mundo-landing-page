import React from 'react';
import { uniqueId } from 'lodash';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';

import config from '@/config';
import RadioButton from '@/components/button/radioButton';
import { SetLanguageVisibleAboutUs, SetLocale } from '@/redux/actions';
import { getRedux } from '@/redux/selectors';
import { keyLanguage } from '@/redux/selectors/selectorsKey';

const LanguageListAboutUs = ({ visible }) => {
  const styles = { display: visible ? 'block' : 'none' };
  const { languageList } = config;
  const dispatch = useDispatch();
  const language = getRedux(keyLanguage);
  const setLanguage = (item) => {
    dispatch(SetLocale(item));
    dispatch(SetLanguageVisibleAboutUs(false));
  };
  const result = languageList.map((item) => (
    <RadioButton
      key={uniqueId()}
      checked={language === item.lang}
      name={item.id}
      onChange={() => setLanguage(item.lang)}
      value={item.lang}
      isLabel
    />
  ));
  return (
    <div className="language-list-aboutUs-container" style={styles}>
      {result}
    </div>
  );
};
LanguageListAboutUs.defualtProps = {
  visible: false,
};
LanguageListAboutUs.propTypes = {
  visible: PropTypes.bool,
};
export default LanguageListAboutUs;
