import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import Button from '@/components/button/button';
import IconSvg from '@/components/icons/iconSvg';
import ItemMenu from '@/components/items/itemMenu';
import ItemMenuTitle from '@/components/items/itemMenuTitle';
import LocaleLanguage from '@/components/language/localeLanguage';
import { getDarkMode } from '@/helpers';
import { iconMenuOpen } from '@/model/icons';
import { getRedux } from '@/redux/selectors';
import { keySrc } from '@/redux/selectors/selectorsKey';
import City from '@/views/modal/city';
import Login from '@/views/login';

const ContentMenu = ({ visible, onClick }) => {
  const src = getRedux(keySrc);
  const styles = { display: visible ? 'block' : 'none' };
  const className = 'content-menu-container__item-title';
  const classNameButton = 'button-principal__item-menu-title';
  const { theme, color } = getDarkMode();
  return (
    <div className="content-menu-container" style={styles}>
      <Button
        className="button-principal__menu-content"
        content={(
          <IconSvg fill={color} stroke={color} {...iconMenuOpen} />
        )}
        height={2}
        onClick={onClick}
        width={4}
      />

      <ItemMenuTitle className={className} classNameButton={classNameButton} title="nameApp" width={11.1} />
      <ItemMenu content={<Login src={src} />} theme={theme} />
      <ItemMenu content={<City />} theme={theme} />
      <ItemMenu content={(<Button content={<FormattedMessage id="leave" />} />)} theme={theme} />
      <ItemMenuTitle className={className} classNameButton={classNameButton} title="discover" width={11.1} />
      <ItemMenu content={<Button content={<FormattedMessage id="ticketPlay" />} />} theme={theme} />
      <ItemMenu content={<Button content={<FormattedMessage id="fantempo" />} />} theme={theme} />
      <ItemMenuTitle className={className} classNameButton={classNameButton} title="language" width={11.1} />
      <ItemMenu content={<LocaleLanguage />} theme={theme} />
    </div>
  );
};

ContentMenu.defaultProps = {
  visible: false,
  onClick: null,
};

ContentMenu.propTypes = {
  visible: PropTypes.bool,
  onClick: PropTypes.func,
};

export default ContentMenu;
