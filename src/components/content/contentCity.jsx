import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import { getDarkMode } from '@/helpers/darkMode';

const ContentCity = ({ colorTitle, listCity }) => {
  const { theme } = getDarkMode();
  const size = theme === 'light' ? '1' : '.1';
  const listStyles = { border: `0.1rem solid rgba(244, 244, 244, ${size})` };

  return (
    <div className="content-city-container">
      <div className="content-city-container__top">
        <div className="content-city-container__top__label" style={{ color: colorTitle }}>
          <FormattedMessage id="city.title" />
        </div>
        <div className="content-city-container__top__list" style={listStyles}>{listCity}</div>
      </div>
    </div>
  );
};

ContentCity.defaultProps = {
  colorTitle: '',
  listCity: null,
};

ContentCity.propTypes = {
  colorTitle: PropTypes.string,
  listCity: PropTypes.element,
};
export default ContentCity;
