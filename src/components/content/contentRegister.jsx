import PropTypes from 'prop-types';
import React from 'react';
import { FormattedMessage } from 'react-intl';

import Input from '@/components/input/input';
import Button from '@/components/button/button';
import { getDarkMode } from '@/helpers/darkMode';
import colors from '@/model/colors';

const ContentRegister = ({ onClick }) => {
  const [email, setEmail] = React.useState(null);
  const [lastname, setLastname] = React.useState(null);
  const [name, setName] = React.useState(null);
  const [password, setPassword] = React.useState(null);
  const [passwordConfirm, setPasswordConfirm] = React.useState(null);
  const { color, theme } = getDarkMode();
  const [styles, setStyles] = React.useState({
    backgroundColor: colors.turquoiseBlue,
  });

  const onMouseOver = () => {
    if (theme === 'light') {
      setStyles({
        backgroundColor: colors.charcoalGrey,
      });
    } else {
      setStyles({
        backgroundColor: colors.transparent,
        border: `.1rem solid ${colors.turquoiseBlue}`,
      });
    }
  };
  return (
    <div className="content-input">
      <Input
        color={color}
        marginTop={2.8}
        onChange={(e) => setName(e.target.value)}
        placeholder="name"
      />
      <Input
        color={color}
        marginTop={2}
        onChange={(e) => setLastname(e.target.value)}
        placeholder="lastName"
      />
      <Input
        color={color}
        marginTop={2}
        onChange={(e) => setEmail(e.target.value)}
        placeholder="email"
        type="email"
      />
      <Input
        color={color}
        marginTop={2}
        placeholder="password"
        onChange={(e) => setPassword(e.target.value)}
        type="password"
      />
      <Input
        color={color}
        marginBottom={1.9}
        marginTop={2}
        placeholder="passwordConfirm"
        onChange={(e) => setPasswordConfirm(e.target.value)}
        type="password"
      />
      <Button
        className="button-principal__login"
        content={<FormattedMessage id="signMeUp" />}
        height={4}
        onClick={() => onClick({
          name, lastname, email, password, passwordConfirm,
        })}
        onMouseOver={onMouseOver}
        onMouseOut={() => setStyles({ backgroundColor: colors.turquoiseBlue })}
        style={styles}
        width={30}
      />
    </div>
  );
};
ContentRegister.defaultProps = {
  onClick: null,
};
ContentRegister.propTypes = {
  onClick: PropTypes.func,
};
export default ContentRegister;
