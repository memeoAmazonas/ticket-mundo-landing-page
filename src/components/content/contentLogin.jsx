import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import Input from '@/components/input/input';
import Button from '@/components/button/button';
import IconLabel from '@/components/icons/iconLabel';
import colors from '@/model/colors';
import iconGoogle from '@/static/images/icon-google.svg';
import iconFacebook from '@/static/images/icon-facebook.svg';
import { getDarkMode } from '@/helpers';

const ContentLogin = ({ onExit, onLogin, onforgotPassword }) => {
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const { color, theme } = getDarkMode();
  const [styles, setStyles] = React.useState({
    backgroundColor: colors.turquoiseBlue,
  });
  const [stylesForgot, setStylesForgot] = React.useState({});
  const [guestStyles, setGuestStyles] = React.useState({ color: colors.turquoiseBlue });
  if (theme === 'light' && stylesForgot === {}) {
    setStylesForgot({ color: colors.blueyGrey });
  } else if (stylesForgot === {}) {
    setStylesForgot({ color: colors.fourthColor });
  }
  const onMouseOutForgot = () => {
    if (theme === 'light') {
      setStylesForgot({ color: colors.blueyGrey });
    } else {
      setStylesForgot({ color: colors.fourthColor });
    }
  };
  const onMouseOver = () => {
    if (theme === 'light') {
      setStyles({
        backgroundColor: colors.charcoalGrey,
      });
    } else {
      setStyles({
        backgroundColor: colors.transparent,
        border: `.1rem solid ${colors.turquoiseBlue}`,
      });
    }
  };
  const onMouseOverGuest = () => {
    if (theme === 'light') {
      setGuestStyles({ color: colors.blueyGrey });
    } else {
      setGuestStyles({ color: colors.fourthColor });
    }
  };
  return (
    <div className="content-input">
      <Input
        color={color}
        marginTop={3}
        onChange={(e) => setEmail(e.target.value)}
        placeholder="email"
        type="email"
      />
      <Input
        color={color}
        marginBottom={1.9}
        marginTop={3}
        onChange={(e) => setPassword(e.target.value)}
        placeholder="password"
        type="password"
      />
      <Button
        className="button-principal__login"
        content={<FormattedMessage id="login" />}
        height={4}
        onClick={() => onLogin({ email, password })}
        onMouseOver={onMouseOver}
        onMouseOut={() => setStyles({ backgroundColor: colors.turquoiseBlue })}
        style={styles}
        width={30}
      />
      <Button
        className="button-principal__forgot-password"
        content={<FormattedMessage id="forgotPassword" />}
        height={4}
        onClick={onforgotPassword}
        onMouseOut={onMouseOutForgot}
        onMouseOver={() => setStylesForgot({ color: colors.turquoiseBlue })}
        style={stylesForgot}
        width={30}
      />
      <div className="login-container__content__icon-network">
        <div className="login-container__content__icon-network__left">
          <IconLabel src={iconGoogle} id="google" />
        </div>
        <div className="login-container__content__icon-network__right">
          <IconLabel src={iconFacebook} id="facebook" />
        </div>
      </div>
      <div className="login-container__content__continue-as-guest">
        <Button
          className="button-principal__continue-as-guest"
          content={<FormattedMessage id="continueAsGuest" />}
          height={2}
          onClick={onExit}
          onMouseOver={onMouseOverGuest}
          onMouseOut={() => setGuestStyles({ color: colors.turquoiseBlue })}
          style={guestStyles}
          width={18.4}
        />
      </div>
    </div>
  );
};
ContentLogin.defaultProps = {
  onExit: null,
  onforgotPassword: null,
  onLogin: null,
};
ContentLogin.propTypes = {
  onExit: PropTypes.func,
  onforgotPassword: PropTypes.func,
  onLogin: PropTypes.func,
};
export default ContentLogin;
