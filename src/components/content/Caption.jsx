import React from 'react';
import PropTypes from 'prop-types';

import Button from '@/components/button/button';
import { getSizeWindow } from '@/helpers';

const Caption = ({
  author, content, isVisibleButton, onClick, position, title, textButton,
}) => {
  const { width } = getSizeWindow();
  let marginTitle = 1.22;
  let marginAuthor = 1.8;
  let marginContent = 2.4;
  if (width <= 890) {
    marginTitle = 1.02;
    marginAuthor = 1.3;
    marginContent = 2.18;
  }

  const getPosition = (posicion) => {
    if (width > 720) {
      switch (posicion) {
        case 'Izquierda':
          return {
            alignItems: 'flex-start',
            textAlign: 'left',
          };
        case 'Centro':
          return {
            alignItems: 'center',
            textAlign: 'center',
          };
        case 'Derecha':
          return {
            alignItems: 'flex-end',
            textAlign: 'right',
          };
        default:
          return {
            alignItems: 'flex-start',
            textAlign: 'left',
          };
      }
    }
    return {
      alignItems: 'center',
      textAlign: 'center',
    };
  };
  const stylesButton = !isVisibleButton ? { display: 'none' } : { display: 'block' };
  const styles = { ...getPosition(position) };
  return (
    <div className="legend-carousel-container" style={styles}>
      <div
        className="legend-carousel-container__title"
        style={{ marginBottom: title ? `${marginTitle}rem` : 0 }}
      >
        {title}
      </div>
      <div
        className="legend-carousel-container__author"
        style={{ marginBottom: author ? `${marginAuthor}rem` : 0 }}
      >
        {author}
      </div>
      <div
        className="legend-carousel-container__content"
        style={{ marginBottom: content ? `${marginContent}rem` : 0 }}
      >
        {content}
      </div>
      <div
        className="legend-carousel-container__button"
        style={stylesButton}
      >
        <Button
          className="button-principal__carousel"
          content={<span>{textButton}</span>}
          onClick={onClick}
        />
      </div>
    </div>
  );
};

Caption.defaultProps = {
  author: null,
  content: null,
  isVisibleButton: true,
  onClick: null,
  position: 'left',
  title: '',
  textButton: '',
};
Caption.propTypes = {
  author: PropTypes.string,
  content: PropTypes.string,
  isVisibleButton: PropTypes.bool,
  onClick: PropTypes.func,
  position: PropTypes.string,
  title: PropTypes.string,
  textButton: PropTypes.string,

};

export default Caption;
