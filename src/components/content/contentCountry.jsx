import React from 'react';
import PropTypes from 'prop-types';
import { isEqual, findIndex } from 'lodash';
import { FormattedMessage } from 'react-intl';

import Image from '@/components/image/image';
import ItemCountryModal from '@/components/items/itemCountryModal';
import config from '@/config';
import { getDarkMode, RedirectTo } from '@/helpers';
import colors from '@/model/colors';
import Countries from '@/model/countries';

const ContentCountry = ({ src, onClick }) => {
  const { countryCode } = config;
  const country = findIndex(Countries, (item) => isEqual(item.countryCode, countryCode));
  const act = Countries[country !== -1 ? country : 0];

  const setCountry = (dataCountry, init, end) => dataCountry.slice(init, end)
    .map((item) => {
      if (item !== act) {
        return (
          <ItemCountryModal
            key={item.name}
            name={item.name}
            nameUrl={item.link}
            onClick={() => {
              RedirectTo(item.link);
            }}
            src={item.src}
            width={item.width}
          />
        );
      }
      return null;
    });
  const { theme } = getDarkMode();
  const color = theme === 'dark' ? colors.fourthColor : colors.charcoalGrey;
  return (
    <div className="modal-init-container">
      <div className="modal-init-container__header">
        <div className="modal-init-container__header__image">
          <Image src={src} height={2.4} width={19.8} />
        </div>
        <span className="modal-init-container__header__label" style={{ color }}>
          <FormattedMessage id="comingIntoHome" />
        </span>
        <span className="modal-init-container__header__label-alternative" style={{ color }}>
          <FormattedMessage id="comingIntoHomeAlternative" />
        </span>
        <div className="modal-init-container__header__default-country">
          <ItemCountryModal
            actually
            name={act.name}
            nameUrl="enter"
            onClick={onClick}
            src={act.src}
            width={act.width}
          />
        </div>
        <span className="modal-init-container__header__label-subtitle" style={{ color }}>
          <FormattedMessage id="selectOtherCountry" />
        </span>
      </div>
      {setCountry(Countries, 0, 4)}
      <div className={`modal-init-container__separator-${theme}`}>
        {setCountry(Countries, 4, 6)}
      </div>
    </div>
  );
};
ContentCountry.defaultProps = {
  onClick: null,
  src: '',
};
ContentCountry.propTypes = {
  onClick: PropTypes.func,
  src: PropTypes.string,
};
export default ContentCountry;
