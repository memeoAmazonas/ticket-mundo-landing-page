import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import { getDarkMode } from '@/helpers';
import colors from '@/model/colors';

const Input = ({
  autoFocus, className, color, height, marginBottom, marginTop, onChange, placeholder, type, width,
}) => {
  const { theme } = getDarkMode();

  const styles = {
    color,
    height: `${height}rem`,
    marginBottom: `${marginBottom}rem`,
    marginTop: `${marginTop}rem`,
    width: `${width}%`,
  };
  return (
    <FormattedMessage id={placeholder}>
      {(place) => (
        <input
          autoFocus={autoFocus}
          className={`${className} input-container-placeholder-${theme}`}
          onChange={onChange}
          placeholder={place}
          style={styles}
          type={type}
        />
      )}
    </FormattedMessage>
  );
};

Input.defaultProps = {
  autoFocus: false,
  className: 'input-container',
  color: colors.charcoalGrey,
  height: 4,
  marginBottom: 0,
  marginTop: 0,
  onChange: null,
  placeholder: '',
  type: 'text',
  width: 100,
};
Input.propTypes = {
  autoFocus: PropTypes.bool,
  className: PropTypes.string,
  color: PropTypes.string,
  height: PropTypes.number,
  marginBottom: PropTypes.number,
  marginTop: PropTypes.number,
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  type: PropTypes.string,
  width: PropTypes.number,
};
export default Input;
