import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import Button from '@/components/button/button';
import { getDarkMode } from '@/helpers/darkMode';
import colors from '@/model/colors';

const LabelInput = ({
  className, title, titleButton, onClick,
}) => {
  const { theme } = getDarkMode();
  const [styles, setStyles] = useState({ color: colors.turquoiseBlue });
  const onMouseOver = () => {
    if (theme === 'light') {
      setStyles({ color: colors.blueyGrey });
    } else {
      setStyles({ color: colors.fourthColor });
    }
  };
  return (
    <div className="login-container__content__legend">
      <span className="login-container__content__legend__label">
        <FormattedMessage id={title} />
      </span>
      <Button
        className={className}
        content={<FormattedMessage id={titleButton} />}
        onClick={onClick}
        onMouseOver={onMouseOver}
        onMouseOut={() => setStyles({ color: colors.turquoiseBlue })}
        style={styles}
        width={11}
      />
    </div>
  );
};
LabelInput.defaultProps = {
  className: 'button-principal__register-login',
  onClick: null,
  title: '',
  titleButton: '',
};
LabelInput.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func,
  title: PropTypes.string,
  titleButton: PropTypes.string,
};
export default LabelInput;
