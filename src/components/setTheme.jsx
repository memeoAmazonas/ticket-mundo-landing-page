import React from 'react';
import { useDispatch } from 'react-redux';

import { hourSleep } from '@/model/constants';
import { ChangeLogo } from '@/redux/actions';
import { getLogo } from '@/helpers';

const SetTheme = () => {
  const dispatch = useDispatch();
  let local;
  if (sessionStorage.getItem('theme') !== null) {
    local = sessionStorage.getItem('theme');
  } else {
    local = (hourSleep.includes(new Date().getHours())) ? 'dark' : 'light';
  }
  const [theme, toggleTheme] = React.useState(local);
  const changeTheme = () => {
    if (theme !== 'dark') {
      sessionStorage.setItem('theme', 'dark');
      toggleTheme('dark');
      document.body.classList.add('__darkmode');
      const logo = getLogo(true);
      dispatch(ChangeLogo(logo));
    } else {
      toggleTheme('light');
      sessionStorage.setItem('theme', 'light');
      document.body.classList.remove('__darkmode');
      const logo = getLogo(false);
      dispatch(ChangeLogo(logo));
    }
  };
  return (
    <label
      htmlFor="darkMode"
      className="switch"
    >
      <input
        id="darkMode"
        type="checkbox"
        defaultChecked={theme}
        onChange={() => changeTheme()}
      />
      <span className="slider round" />
    </label>
  );
};
export default SetTheme;
