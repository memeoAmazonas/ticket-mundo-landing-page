import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import Button from '@/components/button/button';
import { getDarkMode, getSizeWindow } from '@/helpers';
import colors from '@/model/colors';
import { IconDeskSearch, IconMobileSearch } from '@/model/icons';
import { SetColor, SetFind } from '@/redux/actions';
import { getRedux } from '@/redux/selectors';
import { keyIsVisibleSearch, keySetColorIconSearch } from '@/redux/selectors/selectorsKey';
import { SET_COLOR_ICON_SEARCH } from '@/redux/types';

const SearchIcon = () => {
  const { color } = getDarkMode();
  const dispatch = useDispatch();
  const isVisible = getRedux(keyIsVisibleSearch);
  const actuallyColor = getRedux(keySetColorIconSearch);
  useEffect(() => {
    if (!actuallyColor) {
      dispatch(SetColor(SET_COLOR_ICON_SEARCH, color));
    }
  });
  const onClick = () => {
    dispatch(SetFind(!isVisible));
    dispatch(SetColor(SET_COLOR_ICON_SEARCH, color));
    window.scrollTo(0, 0);
  };

  const icon = getSizeWindow().width > 414
    ? <IconDeskSearch color={actuallyColor} /> : <IconMobileSearch color={actuallyColor} />;
  return (
    <div className="search-icon-container">
      <Button
        className="button-principal__search-icon"
        content={icon}
        height={2.5}
        onClick={onClick}
        width={2.5}
        onMouseOver={() => dispatch(SetColor(SET_COLOR_ICON_SEARCH, colors.turquoiseBlue))}
        onMouseOut={() => dispatch(SetColor(SET_COLOR_ICON_SEARCH, color))}
      />
    </div>
  );
};
export default SearchIcon;
