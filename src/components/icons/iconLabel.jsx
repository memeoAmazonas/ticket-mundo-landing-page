import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import Image from '@/components/image/image';
import Button from '@/components/button/button';
import colors from '@/model/colors';

const IconLabel = ({ id, src, onClick }) => {
  const content = (
    <button type="button" className="button-principal__icon-label__content" onClick={onClick}>
      <Image width={2} height={2} src={src} alt={id} marginTop={0.6} />
      <span className="button-principal__icon-label__content__label"><FormattedMessage id={id} /></span>
    </button>
  );
  return (
    <Button
      className="button-principal__icon-label"
      dark={colors.fourthColor}
      light={colors.blueyGrey}
      height={4}
      width={14.5}
      content={content}
    />
  );
};

IconLabel.defaultProps = {
  id: '',
  src: '',
  onClick: null,
};
IconLabel.propTypes = {
  id: PropTypes.string,
  src: PropTypes.string,
  onClick: PropTypes.func,
};

export default IconLabel;
