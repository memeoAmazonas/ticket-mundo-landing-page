import React from 'react';
import { uniqueId } from 'lodash';
import PropTypes from 'prop-types';

const IconSvg = (props) => {
  const {
    height, path, stroke, transform, type, viewBox, width,
  } = props;

  const content = () => {
    switch (type) {
      case 'normal':
        return <path {...props} />;
      case 'path':
        if (path) {
          return (
            <g transform={transform}>
              {path.map((item) => (
                <path key={uniqueId()} stroke={stroke} {...item} />
              ))}
            </g>
          );
        }
        return null;
      case 'line':
        return (
          <g transform={transform}>
            {path.map((item) => (
              <line key={uniqueId()} {...item} />
            ))}
          </g>
        );
      default:
        return null;
    }
  };
  const result = () => (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={width}
      height={height}
      viewBox={viewBox}
    >
      {content()}
    </svg>
  );
  return <>{result()}</>;
};

IconSvg.propTypes = {
  height: PropTypes.string,
  path: PropTypes.array,
  stroke: PropTypes.string,
  transform: PropTypes.string,
  type: PropTypes.string,
  viewBox: PropTypes.string,
  width: PropTypes.string,
};
IconSvg.defaultProps = {
  height: null,
  path: [],
  stroke: null,
  transform: null,
  type: null,
  viewBox: null,
  width: null,
};

export default IconSvg;
