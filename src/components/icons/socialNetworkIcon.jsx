import React from 'react';
import PropTypes from 'prop-types';
import { SocialMediaIconsReact } from 'social-media-icons-react';

import { colors } from '@/model';

const SocialNetworkIcon = ({
  borderWidth, name, size, url, iconSize,
}) => (
  <>
    <SocialMediaIconsReact
      borderColor={colors.iconSocialNetworkColor}
      borderWidth={borderWidth}
      borderStyle="solid"
      icon={name}
      iconColor={colors.iconSocialNetworkColor}
      backgroundColor={colors.transparent}
      iconSize={iconSize}
      roundness="50%"
      url={url}
      size={size}
    />
  </>
);

SocialNetworkIcon.propTypes = {
  borderWidth: PropTypes.string,
  iconSize: PropTypes.string,
  name: PropTypes.string,
  size: PropTypes.string,
  url: PropTypes.string,
};
SocialNetworkIcon.defaultProps = {
  borderWidth: '1',
  iconSize: '3',
  name: '',
  size: '30',
  url: '',
};

export default SocialNetworkIcon;
