import React from 'react';
import PropTypes from 'prop-types';
import { Image } from 'semantic-ui-react';
import { FormattedMessage } from 'react-intl';

const IconBackstage = ({
  height, title, src, subtitle, width,
}) => {
  const styles = {
    height: `${height}rem`,
    width: `${width}rem`,
  };
  return (
    <div className="icon-backstage-container">
      <Image src={src} style={styles} className="icon-backstage-container__image" />
      <div className="icon-backstage-container__content">
        <div className="icon-backstage-container__content__title">
          <FormattedMessage id={title} />
        </div>
        <div className="icon-backstage-container__content__subtitle">
          <FormattedMessage id={subtitle} />
        </div>
      </div>
    </div>
  );
};

IconBackstage.propTypes = {
  height: PropTypes.number,
  src: PropTypes.string,
  subtitle: PropTypes.string,
  title: PropTypes.string,
  width: PropTypes.number,
};
IconBackstage.defaultProps = {
  height: 0,
  src: '',
  subtitle: '',
  title: '',
  width: 0,

};

export default IconBackstage;
