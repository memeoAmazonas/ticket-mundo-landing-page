import React from 'react';
import PropTypes from 'prop-types';
import { Modal as Nmodal } from 'react-bootstrap';

import Image from '@/components/image/image';
import Button from '@/components/button/button';
import IconSvg from '@/components/icons/iconSvg';
import { iconCloseWindow, colors } from '@/model';

const Modal = ({
  content, show, onHide, theme, src, visibleHeader,
}) => {
  const param = iconCloseWindow;
  param.path[0].stroke = theme === 'dark' ? colors.fourthColor : colors.sixthColor;
  param.path[1].stroke = theme === 'dark' ? colors.fourthColor : colors.sixthColor;
  return (
    <Nmodal
      dialogClassName={`modal-container modal-container__${theme}`}
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      {visibleHeader && (
      <Nmodal.Header closeButton onHide={onHide}>
        <div className="modal-header__header-image">
          <Image src={src} height={2.6} width={19} alt={src} />
        </div>
        <Button
          className="modal-header__button-close"
          content={<IconSvg {...iconCloseWindow} />}
          height={2.6}
          onClick={onHide}
          width={3.2}
        />
      </Nmodal.Header>
      )}
      <Nmodal.Body>
        {content}
      </Nmodal.Body>
    </Nmodal>
  );
};
Modal.defaultProps = {
  content: null,
  onHide: null,
  theme: '',
  show: false,
  src: '',
  visibleHeader: true,
};

Modal.propTypes = {
  content: PropTypes.element,
  onHide: PropTypes.func,
  theme: PropTypes.string,
  show: PropTypes.bool,
  src: PropTypes.string,
  visibleHeader: PropTypes.bool,
};
export default Modal;
