import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { concat, uniqueId } from 'lodash';
import { FormattedMessage } from 'react-intl';
import { useDispatch } from 'react-redux';

import Button from '@/components/button/button';
import LanguageListAboutUs from '@/components/language/languagesListAboutUs';
import { getSizeWindow, UseComponentVisible, RedirectTo } from '@/helpers';
import { SetVisibleCategory } from '@/redux/actions';
import { getRedux } from '@/redux/selectors';
import { keyVisibleCategory } from '@/redux/selectors/selectorsKey';

const AboutUsCategory = ({
  items, title, width,
}) => {
  const dispatch = useDispatch();
  const widthSize = getSizeWindow().width;
  const isMobile = widthSize < 723;
  const { ref, isComponentVisible, setIsComponentVisible } = UseComponentVisible(false);
  const isVisibleCategory = getRedux(keyVisibleCategory);
  // todo colocar cuando se arreglen todos los items
  //  const fontSize = widthSize > 860 && widthSize <= 1050 ? '1rem' : '1.2rem';
  const fontSize = '1.2rem';
  const conditionToVisible = isVisibleCategory === title;
  const stylesButton = { width: `${width}rem`, fontSize };
  const languageList = <LanguageListAboutUs key={uniqueId()} visible={isComponentVisible} />;
  const isAccount = title === 'aboutus.accounts';
  const redirectTo = ({ label, url }) => {
    if (label === 'language') {
      setIsComponentVisible(!isComponentVisible);
    } else {
      RedirectTo(url);
    }
  };
  const categoryList = items.map((it) => {
    const button = (
      <Button
        className="button-principal__aboutus-category-item"
        key={uniqueId()}
        onClick={() => (it.external || it.label === 'language' ? redirectTo(it) : null)}
        content={<FormattedMessage id={it.label} />}
        style={stylesButton}
      />
    );
    if (it.external || it.label === 'language') {
      return button;
    }
    return (
      <Link key={uniqueId()} to={it.url}>
        {button}
      </Link>
    );
  });
  const setVisible = () => {
    if (isMobile) {
      if (!conditionToVisible) {
        dispatch(SetVisibleCategory(title));
      } else {
        dispatch(SetVisibleCategory(''));
      }
    }
  };

  const setCategories = () => {
    if (isMobile) {
      if (conditionToVisible) {
        if (isAccount) {
          return categoryList.splice(0, categoryList.length - 1);
        }
        return categoryList;
      }
      return null;
    }
    if (isAccount) {
      return concat(categoryList, languageList);
    }
    return categoryList;
  };

  return (
    <div className="about-us-category-container" ref={ref}>
      <Button
        className="button-principal__aboutus-category-label"
        content={<FormattedMessage id={title} />}
        onClick={() => setVisible()}
        style={stylesButton}
      />
      {setCategories()}
    </div>
  );
};

AboutUsCategory.propTypes = {
  items: PropTypes.array,
  title: PropTypes.string,
  width: PropTypes.number,
};

AboutUsCategory.defaultProps = {
  items: [],
  title: '',
  width: 15.6,
};

export default AboutUsCategory;
