import PropTypes from 'prop-types';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { useDispatch } from 'react-redux';

import Button from '@/components/button/button';
import { colors, ContacUsIcon } from '@/model';
import { SetColor } from '@/redux/actions';
import { SET_COLOR_ICON_HELP_ABOUTUS } from '@/redux/types';
import { keySetColorIconHelpAboutUs } from '@/redux/selectors/selectorsKey';
import { getRedux } from '@/redux/selectors';

const AboutUsContactUs = ({ onClick }) => {
  const dispatch = useDispatch();
  const color = getRedux(keySetColorIconHelpAboutUs);
  React.useEffect(() => {
    if (!color) {
      dispatch(SetColor(SET_COLOR_ICON_HELP_ABOUTUS, colors.thirteenth));
    }
  });
  return (
    <div className="about-us-contact-us-container">
      <div className="about-us-contact-us-container__title">
        <FormattedMessage id="help" />
      </div>
      <p className="about-us-contact-us-container__content">
        <FormattedMessage id="help.content" />
      </p>
      <button
        type="button"
        className="about-us-contact-us-container__image"
        onMouseOut={() => dispatch(SetColor(SET_COLOR_ICON_HELP_ABOUTUS, colors.thirteenth))}
        onMouseOver={() => dispatch(SetColor(SET_COLOR_ICON_HELP_ABOUTUS, colors.turquoiseBlue))}
      >
        <ContacUsIcon color={color} />
      </button>
      <Button
        content={<FormattedMessage id="contactUs" />}
        className="button-principal__contact-us-last-title"
        onClick={onClick}
      />
    </div>
  );
};

AboutUsContactUs.propTypes = {
  onClick: PropTypes.func,
};
AboutUsContactUs.defaultProps = {
  onClick: null,
};

export default AboutUsContactUs;
