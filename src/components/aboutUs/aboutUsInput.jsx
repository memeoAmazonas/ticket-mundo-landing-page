import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import Button from '@/components/button/button';
import Input from '@/components/input/input';

const AboutUsInput = ({ onChange }) => {
  const subscribe = () => { };
  return (
    <div className="about-us-input-container">
      <div className="about-us-input-container__label">
        <FormattedMessage id="find.out.events" />
      </div>
      <div className="about-us-input-container__field">
        <Input
          className="about-us-input-container__field__input"
          onChange={onChange}
          placeholder="ingress.email"
          type="email"
        />
        <Button
          className="button-principal__aboutus-suscribe"
          content={<FormattedMessage id="suscribe" />}
          onClick={subscribe}
        />
      </div>
    </div>
  );
};

AboutUsInput.propTypes = { onChange: PropTypes.func };
AboutUsInput.defaultProps = { onChange: null };

export default AboutUsInput;
