import React from 'react';
import PropTypes from 'prop-types';

const Separator = ({ horizontal, color, width }) => {
  const styles = {
    color,
    height: !horizontal ? `${width}rem` : '0.1rem !important',
    width: horizontal ? `${width}rem` : '0.1rem',
  };
  return (
    <div className="separator-horizontal" style={styles} />
  );
};

Separator.defaultProps = {
  color: '',
  horizontal: false,
  width: 0,
};

Separator.propTypes = {
  color: PropTypes.string,
  horizontal: PropTypes.bool,
  width: PropTypes.number,
};
export default Separator;
