import React from 'react';
import { concat, filter } from 'lodash';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { FormattedMessage } from 'react-intl';

import Button from '@/components/button/button';
import IconSvg from '@/components/icons/iconSvg';
import ItemCity from '@/components/items/itemCity';
import Modal from '@/components/modal';
import { getDarkMode, getSizeWindow } from '@/helpers';
import colors from '@/model/colors';
import { iconCloseWindow } from '@/model/icons';
import { GetEventByCity, SetActuallyCity, GetEventNearBy } from '@/redux/actions';
import {
  keyLocationActuallyCity, keyLocationInfoCountry, keyLocationCityList,
} from '@/redux/selectors/selectorsKey';
import { getRedux } from '@/redux/selectors';

const EventListCity = ({ setVisible, visible }) => {
  const dispatch = useDispatch();
  const { theme } = getDarkMode();
  const { width } = getSizeWindow();
  const infoCountry = getRedux(keyLocationInfoCountry);
  const cityList = getRedux(keyLocationCityList);
  const actuallyCity = getRedux(keyLocationActuallyCity);
  const ref = React.createRef();
  const defaulCity = { Id: -1, Ciudad: 'Cerca de ti' };
  const handleSearch = (item) => {
    dispatch(SetActuallyCity(item));
    if (item.Id !== -1) {
      dispatch(GetEventByCity({ idCity: item.Id }));
    } else {
      const { latitude, longitude } = infoCountry;
      dispatch(GetEventNearBy({
        latitude,
        longitude,
      }));
    }
    ref.current.scrollIntoView({
      behavior: 'smooth',
      block: 'nearest',
    });
    setVisible();
  };
  React.useEffect(() => {
    if (cityList && actuallyCity === undefined) {
      dispatch(SetActuallyCity(defaulCity));
    }
  }, [cityList, actuallyCity, dispatch, defaulCity]);
  const onSelectImage = () => {
    if (actuallyCity !== undefined && actuallyCity.Id !== -1) {
      const head = filter(cityList, (it) => it.Id === actuallyCity.Id);
      const tail = filter(cityList, (it) => it.Id !== actuallyCity.Id);
      return concat(head, tail);
    }
    return cityList;
  };
  const listCity = () => {
    if (actuallyCity !== undefined && cityList !== undefined) {
      return concat(defaulCity, onSelectImage()).map((item) => (
        <ItemCity
          key={item.Id}
          checked={actuallyCity.Id === item.Id}
          name={item.Ciudad}
          onChange={() => handleSearch(item)}
          value={actuallyCity.Id}
        />
      ));
    }
    return null;
  };
  let colorTitle;
  let backgroundColor;
  if (theme === 'dark') {
    colorTitle = colors.fourthColor;
    backgroundColor = colors.charcoalGrey;
  } else {
    colorTitle = colors.charcoalGrey;
    backgroundColor = colors.fourthColor;
  }
  const param = iconCloseWindow;
  param.path[0].stroke = theme === 'dark' ? colors.fourthColor : colors.blueyGrey;
  param.path[1].stroke = theme === 'dark' ? colors.fourthColor : colors.blueyGrey;
  param.width = '20';
  param.height = '20';
  const content = (
    <>
      <div className="label-container-list-city">
        <span className="label-container-list-city__label col-10" style={{ color: colorTitle }}>
          <FormattedMessage id={actuallyCity && actuallyCity.Id === -1 ? 'event' : 'event.in'} />
        </span>
        <Button
          className="button-principal__close-modal-city"
          content={<IconSvg {...iconCloseWindow} />}
        />
      </div>
      <div className={`event-list-city-container box-shadow-${theme}`} ref={ref} style={{ backgroundColor }}>
        {listCity()}
      </div>
    </>
  );
  if (width > 590) {
    return (
      <>
        { content }
      </>
    );
  }
  if (width < 591) {
    return (
      <Modal
        visibleHeader={false}
        show={visible}
        content={content}
        onHide={() => setVisible(false)}
        theme={theme}
      />
    );
  }
  return null;
};
EventListCity.propTypes = {
  visible: PropTypes.bool.isRequired,
  setVisible: PropTypes.func.isRequired,
};

export default EventListCity;
