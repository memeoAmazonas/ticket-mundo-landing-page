import { findIndex, isEqual } from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { faAngleDown, faAngleUp } from '@fortawesome/free-solid-svg-icons';

import config from '@/config';
import ItemCountry from '@/components/items/itemCountry';
import { RedirectTo, UseComponentVisible } from '@/helpers';
import Countries from '@/model/countries';

const CountryList = ({ backgroundColor }) => {
  const { ref, isComponentVisible, setIsComponentVisible } = UseComponentVisible(false);
  const { countryCode } = config;
  const [, setDisplay] = React.useState(false);
  const country = findIndex(Countries, (item) => isEqual(item.countryCode, countryCode));
  const act = Countries[country !== -1 ? country : 0];
  const content = (Countries)
    .map((item) => {
      if (item !== act) {
        return (
          <ItemCountry
            key={item.name}
            name={item.name}
            onClick={() => {
              RedirectTo(item.link);
              setDisplay(false);
            }}
            src={item.src}
          />
        );
      }
      return null;
    });
  const styles = { display: isComponentVisible ? 'flex' : 'none', backgroundColor };
  return (
    <div className="country-list-container" ref={ref}>
      <div className="country-list-container__actually">
        <ItemCountry
          icon={isComponentVisible ? faAngleDown : faAngleUp}
          name={act.name}
          src={act.src}
          onClick={() => setIsComponentVisible(!isComponentVisible)}
          visible
        />
      </div>

      <div className="country-list-container__list" style={styles}>
        {content}
      </div>
    </div>
  );
};
CountryList.defaultProps = {
  backgroundColor: '',
};
CountryList.propTypes = {
  backgroundColor: PropTypes.string,
};
export default CountryList;
