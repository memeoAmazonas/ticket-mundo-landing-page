const en = require('./language/en.json');
const es = require('./language/es.json');

const strings = {
  en,
  es,
};
export default strings;
