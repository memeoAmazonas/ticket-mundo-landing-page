import Home from '@/views/home/home';
import ContactUs from '@/views/contactUs/contactUs';
import SellWithUs from '@/views/sellWithUs/sellWithUs';
import BrandResources from '@/views/brandResources/brandResources';
import DetailContact from './views/contactUs/detail/DetailContactUs';

const routes = [
  {
    path: '/',
    component: Home,
  },
  {
    path: '/contact-us',
    component: ContactUs,
  },
  {
    path: '/contact-us/detail/:detail',
    component: DetailContact,
  },
  {
    path: '/sell-with-us',
    component: SellWithUs,
  },
  {
    path: '/brand-resources',
    component: BrandResources,
  },
];
export default routes;
