import React from 'react';
import { useDispatch } from 'react-redux';
import { FormattedMessage } from 'react-intl';

import { Button, IconSvg } from '@/components';
import { getSizeWindow, setCookie, setSessionItem } from '@/helpers';
import { colors, iconCloseWindow } from '@/model';
import { SetCookie } from '@/redux/actions';
import { getRedux } from '@/redux/selectors';
import { keyAcceptCookie } from '@/redux/selectors/selectorsKey';

const Cookies = () => {
  const dispatch = useDispatch();
  const cookie = getRedux(keyAcceptCookie);

  const param = iconCloseWindow;
  const { width } = getSizeWindow();
  param.path[0].stroke = colors.fourthColor;
  param.path[1].stroke = colors.fourthColor;
  if (width > 590) {
    param.width = '13';
    param.height = '13';
  } else {
    param.width = '13';
    param.height = '13';
  }
  const setCookies = () => {
    setCookie('setCookie', false);
    dispatch(SetCookie(false));
    setSessionItem('cookie', 'isCookie');
  };
  const setLocalCookie = () => {
    dispatch(SetCookie(false));
    setSessionItem('cookie', 'isCookie');
  };
  const classnameCloseButton = width > 590 ? 'col-2' : 'col-1';
  const classnameMessage = width > 590 ? 'col' : 'col-11';
  if (cookie === false) {
    return null;
  }
  return (
    <div className="cookies-container">
      <div className="cookies-container__content view-container__with-margin">
        <div className={`cookies-container__content__message ${classnameMessage}`}>
          <div className="cookies-container__content__message-label">
            <FormattedMessage
              id="cookie.message"
            />
          </div>
          <div className="cookies-container__content__message-button">
            <Button
              className="button-principal__cookie-principal-close"
              content={(
                <FormattedMessage
                  id="accept"
                />
            )}
              onClick={setCookies}
            />
          </div>
        </div>
        <div className={`cookies-container__content__button ${classnameCloseButton}`}>
          <Button
            className="button-principal__cookie-close"
            content={<IconSvg {...param} />}
            onClick={() => setLocalCookie()}
          />
        </div>
      </div>
    </div>
  );
};
export default Cookies;
