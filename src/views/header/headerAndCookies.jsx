import React from 'react';
import { useDispatch } from 'react-redux';

import { getCookie, getSessionItem, useDocumentScrollThrottled } from '@/helpers';
import { SetCookie } from '@/redux/actions';
import { getRedux } from '@/redux/selectors';
import { keyAcceptCookie, keyIsVisibleSearch } from '@/redux/selectors/selectorsKey';
import Header from '../layout/header';
import Cookies from './cookies';

const HeaderAndCookies = () => {
  const dispatch = useDispatch();
  const isVisibleSearch = getRedux(keyIsVisibleSearch);
  const cookie = getRedux(keyAcceptCookie);
  const [shouldHideHeader, setShouldHideHeader] = React.useState(false);
  useDocumentScrollThrottled((callbackData) => {
    const { currentScrollTop } = callbackData;

    setTimeout(() => {
      setShouldHideHeader(currentScrollTop > 0);
    }, 200);
  });
  React.useEffect(() => {
    if (!cookie && !getSessionItem('cookie')) {
      if (getCookie('setCookie')) {
        dispatch(SetCookie(false));
      } else {
        dispatch(SetCookie(true));
      }
    }
  });

  const onScroll = shouldHideHeader ? 'on-scroll' : '';
  if (!cookie && !isVisibleSearch) {
    return (
      <div className={onScroll}>
        <Header />
      </div>
    );
  }
  if (!isVisibleSearch && cookie) {
    return (
      <div className={onScroll}>
        <Cookies />
        <Header />
      </div>
    );
  }
  if (isVisibleSearch && cookie) {
    return <Cookies />;
  }
  return null;
};

export default HeaderAndCookies;
