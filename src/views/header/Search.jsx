import React from 'react';
import useKey from 'use-key-hook';
import { useDispatch } from 'react-redux';
import { uniqueId, concat } from 'lodash';
import { FormattedMessage } from 'react-intl';

import { Button, Input, ItemEventSearch } from '@/components';
import { getDarkMode, getSizeWindow, setDataEvents } from '@/helpers';
import { colors, IconCloseWindow } from '@/model';
import { GetcontentSearch, ResetSearch, SetFind } from '@/redux/actions';
import { eventPosterMulticarousel } from '@/redux/api/urls';
import { getRedux } from '@/redux/selectors';
import { keyIsVisibleSearch, keySearchContent } from '@/redux/selectors/selectorsKey';

const Search = () => {
  const { theme } = getDarkMode();
  const size = getSizeWindow();
  const [content, setContent] = React.useState('');
  const dispatch = useDispatch();
  const isVisible = getRedux(keyIsVisibleSearch);
  const items = getRedux(keySearchContent);
  const { width } = setDataEvents(size.width);
  useKey(() => {
    dispatch(ResetSearch());
    dispatch(SetFind(false));
  }, {
    detectKeys: [27],
  });
  const color = theme === 'light' ? colors.charcoalGrey : colors.fourthColor;
  const colorMessage = theme === 'light' ? colors.blueyGrey : colors.fourthColor;
  const findEvent = (value) => {
    setContent(value);
    dispatch(GetcontentSearch({ content: value }));
  };
  const result = items ? items.map((item) => {
    if (item.EventoNombre.toLocaleUpperCase()
      .includes(content.toLocaleUpperCase())) {
      return (
        <ItemEventSearch
          city={item.Ciudad}
          key={uniqueId()}
          src={eventPosterMulticarousel(item.EventoId)}
          pattern={content}
          name={item.EventoNombre}
          url={item.Url}
        />
      );
    }
    return null;
  }) : null;

  // TODO recuerda que ya el segundo mensaje cuando esta vacia
  //  la busqueda esta listo, debes solo poner la clase
  //  search-container__content__not-search-success display flex

  return (
    <div
      className="search-container"
      style={{
        minHeight: `${size.height}px`,
      }}
    >
      <div
        className="search-container__content"
        style={{ minHeight: `${size.height}px !important` }}
      >
        <div className="search-container__content__header">
          <div className="search-container__content__header__label">
            <FormattedMessage id="findExperience" />
          </div>
          <Button
            className="button-principal__search-close"
            content={<IconCloseWindow height={22} width={22} />}
            height={3.2}
            onClick={() => {
              dispatch(SetFind(!isVisible));
              dispatch(ResetSearch());
            }}

            width={3.2}
          />
        </div>
        <div className="search-container__content__input">
          <Input
            autoFocus
            color={color}
            className="search-container__content__input-input"
            onChange={(e) => {
              findEvent(e.target.value);
            }}
            placeholder="search"
          />
        </div>
        <div className="view-container__with-margin search-container__content__content">
          {result && concat(result, <div key={uniqueId()} className="last-child-card" style={{ width }} />)}
        </div>
        {(!result || content === '')
        && (
          <span
            className="search-container__content__message"
            style={{ color: colorMessage }}
          >
            <FormattedMessage id="search.placeholder" />
          </span>
        )}
        {(items && items.length === 0 && content.length > 2)
        && (
          <div>
            <>
              <span className="search-container__content__message" style={{ color }}>
                <FormattedMessage id="search.placeholder.empty" />
                <span className="search-container__content__message-subtitle">
                  &nbsp;&quot;
                  {content}
                  &quot;
                </span>
              </span>
            </>
            <div className="search-container__content__not-search-success">
              <span style={{ color: colorMessage }}><FormattedMessage id="search.placeholder.not.result" /></span>
              <Button
                className="button-principal__search-not-result"
                content={<FormattedMessage id="search.placeholder.not.result.button" />}
                onClick={null}
              />
            </div>
          </div>
        )}
      </div>
    </div>
  );
};
export default Search;
