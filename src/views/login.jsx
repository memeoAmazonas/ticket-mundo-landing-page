import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { FormattedMessage } from 'react-intl';

import Modal from '@/components/modal';
import Image from '@/components/image/image';
import Button from '@/components/button/button';
import { getDarkMode } from '@/helpers/darkMode';
import LabelInput from '@/components/input/labelInput';
import ContentLogin from '@/components/content/contentLogin';
import ContentRegister from '@/components/content/contentRegister';
import { useDispatch } from 'react-redux';
import { SetLoadingSkeleton } from '@/redux/actions';

const Login = ({ src }) => {
  const dispatch = useDispatch();

  const { color, theme } = getDarkMode();
  const [visible, setVisible] = useState(false);
  const [register, setRegister] = useState(false);
  const openLogin = () => {
    setVisible(!visible);
    setRegister(false);
  };
  /*
  todo
  call api register and login
   */
  const registerOnClick = (payload) => {
    const {
      name, lastname, email, password, passwordConfirm,
    } = payload;
    setRegister(!register);
  };

  const onLogin = (payload) => {
    const { email, password } = payload;
  };
  const onforgotPassword = () => { null; };

  let titleButton;
  let title;
  let className;
  if (register) {
    titleButton = 'login';
    title = 'haveAccount';
    className = 'button-principal__register';
  } else {
    titleButton = 'signup';
    title = 'notHaveAccount';
    className = 'button-principal__register-login';
  }
  const body = (
    register
      ? <ContentRegister src={src} onClick={registerOnClick} />
      : (
        <ContentLogin
          onExit={() => setVisible(!visible)}
          onforgotPassword={onforgotPassword}
          onLogin={onLogin}
          src={src}
        />
      )
  );
  const content = (
    <div className="login-container__content">
      <div className="login-container__content__image-title-login">
        <Image height={2.4} marginBottom={3} src={src} width={19.8} alt="logo" />
      </div>
      <div className="login-container__content__label-title-login" style={{ color }}>
        <span>
          <FormattedMessage id={register ? 'signup' : 'login.login'} />
        </span>
      </div>
      <LabelInput
        onClick={() => setRegister(!register)}
        title={title}
        titleButton={titleButton}
        className={className}
      />
      {body}
    </div>
  );
  return (
    <div className="login-container">
      <Button
        className="button-principal__header-login"
        content={<FormattedMessage id="login" />}
        width={9}
        height={2}
        onClick={() => openLogin()}
      />
      <Modal
        show={visible}
        content={content}
        onHide={() => {
          setVisible(!visible);
          dispatch(SetLoadingSkeleton(false));
        }}
        theme={theme}
        src={src}
      />
    </div>
  );
};
Login.defaultProps = {
  src: '',
};
Login.propTypes = {
  src: PropTypes.string,
};
export default Login;
