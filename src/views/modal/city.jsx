import PropTypes from 'prop-types';
import { concat } from 'lodash';
import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';

import Button from '@/components/button/button';
import ContentCity from '@/components/content/contentCity';
import ItemCity from '@/components/items/itemCity';
import Modal from '@/components/modal';
import colors from '@/model/colors';
import { GetEventByCity, SetActuallyCity, GetEventNearBy } from '@/redux/actions';
import { getDarkMode } from '@/helpers';
import {
  keyLocationActuallyCity, keyLocationInfoCountry, keyLocationCityList,
} from '@/redux/selectors/selectorsKey';
import { getRedux } from '@/redux/selectors';

const City = ({ label }) => {
  const dispatch = useDispatch();
  const { theme } = getDarkMode();
  const [visible, setVisible] = useState(false);
  const infoCountry = getRedux(keyLocationInfoCountry);
  const cityList = getRedux(keyLocationCityList);
  const actuallyCity = getRedux(keyLocationActuallyCity);
  const defaulCity = { Id: -1, Ciudad: 'Cerca de ti' };
  let colorTitle;
  let colorItem;
  if (theme === 'dark') {
    colorTitle = colors.fourthColor;
    colorItem = colors.firstColor;
  } else {
    colorTitle = colors.charcoalGrey;
    colorItem = colors.thirdColor;
  }
  useEffect(() => {
    if (cityList && actuallyCity === undefined) {
      dispatch(SetActuallyCity(defaulCity));
    }
  }, [cityList, actuallyCity, dispatch, defaulCity]);
  const handleSearch = (item) => {
    dispatch(SetActuallyCity(item));
    if (item.Id !== -1) {
      dispatch(GetEventByCity({ idCity: item.Id }));
    } else {
      const { latitude, longitude } = infoCountry;
      dispatch(GetEventNearBy({
        latitude,
        longitude,
      }));
    }
  };
  const listCity = () => {
    if (actuallyCity !== undefined && cityList !== undefined) {
      return concat(defaulCity, cityList).map((item) => (
        <ItemCity
          key={item.Id}
          checked={actuallyCity.Id === item.Id}
          name={item.Ciudad}
          onChange={() => handleSearch(item)}
          value={actuallyCity.Id}
        />
      ));
    }
    return null;
  };

  const content = (
    <div className="city-container_content">
      <ContentCity
        key="list"
        colorItem={colorItem}
        colorTitle={colorTitle}
        listCity={<>{ listCity()}</>}
        onExit={() => setVisible(false)}
      />
    </div>
  );
  return (
    <div className="login-container">
      <Button
        content={(
          actuallyCity
          && (
          <span>
            {actuallyCity.Ciudad}
          </span>
          )
)}
        height={2}
        onClick={() => setVisible(true)}
        width={20}
      />
      <Modal
        show={visible}
        content={content}
        onHide={() => setVisible(false)}
        theme={theme}
      />
    </div>
  );
};
City.defaultProps = { label: 'city' };
City.propTypes = { label: PropTypes.string };

export default City;
