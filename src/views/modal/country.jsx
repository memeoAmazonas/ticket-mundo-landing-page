import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import Modal from '@/components/modal';
import Button from '@/components/button/button';
import ContentCountry from '@/components/content/contentCountry';

const Country = (props) => {
  const {
    setCountry, country, theme, src, visibleButton,
  } = props;
  const display = visibleButton ? 'block' : 'none';
  return (
    <>
      <div style={{ display }}>
        <Button
          className="button-principal__country-container"
          content={<FormattedMessage id="country" />}
          onClick={setCountry}
        />
      </div>
      <Modal
        content={(
          <ContentCountry
            onClick={setCountry}
            src={src}
          />
      )}
        onHide={setCountry}
        show={country}
        theme={theme}
      />
    </>
  );
};

Country.defaultProps = {
  country: false,
  setCountry: null,
  src: '',
  theme: '',
  visibleButton: false,
};
Country.propTypes = {
  country: PropTypes.bool,
  setCountry: PropTypes.func,
  src: PropTypes.string,
  theme: PropTypes.string,
  visibleButton: PropTypes.bool,
};
export default Country;
