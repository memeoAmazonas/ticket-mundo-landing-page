import React from 'react';
import { uniqueId } from 'lodash';
import { Link } from 'react-router-dom';
import { Image } from 'semantic-ui-react';
import { FormattedMessage } from 'react-intl';

import {
  AboutUsCategory, AboutUsContactUs, Button, Separator, SocialNetworkIcon,
} from '@/components';
import { getSizeWindow, RedirectTo, setWidthCategory } from '@/helpers';
import { colors, AboutUsListCategory, SocialMediaList } from '@/model';
import srcIos from '@/static/images/appstore-icon.svg';
import srcPlaystore from '@/static/images/playstore-icon.svg';

export default () => {
  const size = getSizeWindow().width;
  const categoryList = AboutUsListCategory.map((item) => (
    <AboutUsCategory
      key={uniqueId()}
      title={item.label}
      items={item.items}
      width={setWidthCategory(size, item.label === 'aboutus.accounts')}
    />
  ));
  const socialMedia = SocialMediaList.map((item) => (
    <SocialNetworkIcon
      key={item.link}
      name={item.name}
      url={item.link}
    />
  ));
  const setInternal = () => null;

  return (
    <div className="about-us-container">
      <div className="about-us-container__separator">
        <Separator horizontal />
      </div>
      <div className="about-us-container__content view-container__with-margin">
        <div className="about-us-container__content__category">
          {categoryList}
        </div>
        <div className="about-us-container__content__separator">
          <Separator color={colors.charcoalGrey} width={size > 860 ? 14 : 10} />
        </div>
        {/*
          TODO colocar cuando se implemente el input
            <div className="about-us-container__content__input-button">
              <AboutUsInput onChange={(e) => setEmail(e.target.value)} />
            </div>
        */}
        <Link to="/contact-us">
          <div className="about-us-container__content__contact-us">
            <AboutUsContactUs onClick={() => setInternal()} />
          </div>
        </Link>
        <div className="about-us-container__content__backstage">
          <span className="about-us-container__content__backstage__label">
            <FormattedMessage id="ticketmundo.backstage.app" />
          </span>
          <div className="about-us-container__content__backstage__button">
            <Button
              className="button-principal__about-us-back-stage-app"
              content={<Image src={srcIos} />}
              onClick={() => RedirectTo('')}
            />
            <Button
              className="button-principal__about-us-back-stage-app"
              content={<Image src={srcPlaystore} />}
              onClick={() => RedirectTo('')}
            />
          </div>
        </div>
        <div className="about-us-container__content__social-media">
          {socialMedia}
        </div>
      </div>
    </div>
  );
};
