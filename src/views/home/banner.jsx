import React from 'react';
import Carousel from 'react-bootstrap/Carousel';
import { useDispatch, useSelector } from 'react-redux';

import { Caption } from '@/components';
import { getSizeWindow, RedirectTo } from '@/helpers';
import { GetHeader } from '@/redux/actions';
import getSelector from '@/redux/selectors';
import { keyItemsHeader } from '@/redux/selectors/selectorsKey';

const Banner = () => {
  const dispatch = useDispatch();
  const items = useSelector((state) => getSelector(state, keyItemsHeader));
  const { width } = getSizeWindow();
  React.useEffect(() => {
    if (!items) {
      dispatch(GetHeader());
    }
  });
  const content = !items ? [] : (items.map((item) => {
    const image = width > 590 ? item.UrlBanner : item.UrlBannerMobile;
    let itemCarouselProps = null;
    if (!item.Boton) {
      itemCarouselProps = () => RedirectTo(item.BotonUrl);
    }
    return (
      <Carousel.Item
        key={item.Id}
        style={{ backgroundImage: `url(${image})`, cursor: !item.Boton ? 'pointer' : 'auto' }}
        onClick={itemCarouselProps}
      >
        <Carousel.Caption className="view-container__with-margin">
          <Caption
            title={item.EventoNombre}
            author={item.TextoPrincipal}
            content={item.TextoSecundario}
            position={item.Posicion}
            isVisibleButton={item.Boton}
            onClick={item.Boton ? () => RedirectTo(item.BotonUrl) : null}
            textButton={item.TextoBoton}
          />
        </Carousel.Caption>
      </Carousel.Item>
    );
  }));
  return (
    <div>
      <Carousel interval={6500} fade pauseOnHover>
        {items && content}
      </Carousel>
    </div>
  );
};
export default Banner;
