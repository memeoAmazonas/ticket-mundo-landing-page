import React from 'react';
import { Image } from 'semantic-ui-react';
import { FormattedMessage } from 'react-intl';

import { Button, TkPlayImage } from '@/components';
import { getSizeWindow, RedirectTo } from '@/helpers';
import { TKPLAY } from '@/redux/api/urls';
import src from '@/static/images/logo-tk-play.svg';
import { useDispatch } from 'react-redux';
import { getRedux } from '@/redux/selectors';
import { keyImagesTKMPlay } from '@/redux/selectors/selectorsKey';
import { setAction } from '@/redux/actions';
import { GET_IMAGES_TKMPLAY } from '@/redux/types';

const TkPlay = () => {
  const sizeWindow = getSizeWindow();
  const isDisplay = sizeWindow.width > 860;
  const dispatch = useDispatch();
  const images = getRedux(keyImagesTKMPlay);
  React.useEffect(() => {
    if (!images) {
      dispatch(setAction(GET_IMAGES_TKMPLAY));
    }
  });
  const idLegendTitle = sizeWindow.width > 722 ? 'ticketPlay.title' : 'ticketPlay.title.responsive';
  if (images) {
    return (
      <div className="tk-play-container">
        <div className="view-container__with-margin tk-play-container__content">
          <div className="tk-play-container__content__legend">
            <Image
              alt="ticketmundo play"
              className="tk-play-container__content__legend__logo"
              src={src}
            />
            <div className="tk-play-container__content__legend__title">
              <FormattedMessage id={idLegendTitle} />
            </div>
            <div className="tk-play-container__content__legend__subtitle">
              <FormattedMessage id="ticketPlay.subtitle" />
            </div>
            <div className="tk-play-container__content__legend__image">
              <TkPlayImage src={images[0].ImageUrl} display url={images[0].Url} />
            </div>
            <Button
              className="button-principal__tkplay"
              content={<FormattedMessage id="ticketPlay.goto" />}
              onClick={() => RedirectTo(TKPLAY)}
            />
          </div>
          <div className="tk-play-container__content__image">
            <TkPlayImage src={images[0].ImageUrl} display url={images[0].Url} />
            <TkPlayImage src={images[1].ImageUrl} display={isDisplay} url={images[1].Url} />
          </div>
        </div>
      </div>
    );
  }
  return null;
};
export default TkPlay;
