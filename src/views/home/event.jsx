import React from 'react';
import { useDispatch } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { uniqueId, concat, range } from 'lodash';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusCircle, faMinusCircle } from '@fortawesome/free-solid-svg-icons';

import { Button, EventListCity, ItemCarEvent } from '@/components';
import {
  setNumberItem, RedirectTo, getSizeWindow, getDarkMode,
  setDataEvents, UseComponentVisible,
} from '@/helpers';
import {
  colors, agotado, Gratuito, ListaEspera, RecienAnunciado,
} from '@/model';
import { SetColor } from '@/redux/actions';
import { eventPosterMulticarousel } from '@/redux/api/urls';
import { getRedux } from '@/redux/selectors';
import {
  keyNearByCarousel,
  keySetColorIconEvent,
  keyLocationActuallyCity,
} from '@/redux/selectors/selectorsKey';
import { SET_COLOR_ICON_EVENT } from '@/redux/types';

const Event = () => {
  const dispatch = useDispatch();
  const { theme } = getDarkMode();
  const sizeWindow = getSizeWindow();
  const width = setNumberItem(sizeWindow, 9, 9, 6, 4);
  const widthLabel = setDataEvents(sizeWindow.width).width;
  const [max, setMax] = React.useState(width);
  const eventsNearBy = getRedux(keyNearByCarousel);
  const color = getRedux(keySetColorIconEvent);
  const actuallyCity = getRedux(keyLocationActuallyCity);
  const { ref, isComponentVisible, setIsComponentVisible } = UseComponentVisible(false);
  React.useEffect(() => {
    if (!color) {
      dispatch(SetColor(SET_COLOR_ICON_EVENT, colors.turquoiseBlue));
    }
  });
  const backgroundColor = theme === 'light' ? colors.charcoalGrey : colors.fourthColor;
  const content = eventsNearBy.map((item) => {
    // TODO set label multiplevenue
    let iconSrc;
    let iconLabel;

    if (item.agotado && !iconSrc) {
      iconSrc = agotado;
      iconLabel = 'soldout';
    }
    if (item.RecienAnunciado && !iconSrc) {
      iconSrc = RecienAnunciado;
      iconLabel = 'justannounced';
    }
    if (item.ListaEspera && !iconSrc) {
      iconSrc = ListaEspera;
      iconLabel = 'waitlist';
    }
    if (item.Gratuito && !iconSrc) {
      iconSrc = Gratuito;
      iconLabel = 'free';
    }

    const date = new Date(item.funcionfecha);
    return (
      <ItemCarEvent
        iconLabel={iconLabel}
        srcIcon={iconSrc}
        day={date.getDate()}
        key={uniqueId()}
        src={eventPosterMulticarousel(item.EventoId)}
        month={date.getMonth() + 1}
        name={item.EventoNombre}
        onclick={() => RedirectTo(item.Url)}
        ubication={item.RecintoNombre}
      />
    );
  });

  const visibleMoreSugestion = eventsNearBy && eventsNearBy.length > 8;
  const sugestion = () => {
    if (content.length > max) {
      const diference = width === 9 ? 24 : 2;
      setMax(max + diference);
    } else {
      window.scrollTo(0, 1000);
      setMax(width);
    }
  };
  const onMouseOver = () => {
    const colorOver = theme === 'light' ? colors.charcoalGrey : colors.fourthColor;
    dispatch(SetColor(SET_COLOR_ICON_EVENT, colorOver));
  };
  const button = sizeWindow.width > 723 ? (
    <div className="event-container__button__icon">
      <span
        className="event-container__button-label"
        style={{ color }}
        onMouseOver={onMouseOver}
        onMouseOut={() => dispatch(SetColor(SET_COLOR_ICON_EVENT, colors.turquoiseBlue))}
      >
        <FontAwesomeIcon
          icon={(content.length > max) ? faPlusCircle : faMinusCircle}
          style={{ color }}
        />
        <FormattedMessage id="suggestion" />
      </span>
    </div>
  ) : (
    <div className="event-container__button__label">
      {range(3)
        .map((item) => (
          <div
            key={item}
            style={{ backgroundColor }}
            className="event-container__button__label__point"
          />
        ))}

    </div>
  );
  const actuallyLabel = () => {
    if (actuallyCity && actuallyCity.Id === -1) {
      return 'event';
    }
    return 'event.in';
  };
  return (
    <div className="event-container">
      <div className="event-container__content">
        <div
          className="event-container__content__label"
          style={{ width: sizeWindow.width > 722 ? '100%' : widthLabel }}
        >
          <div className={`event-container__${theme}__button`}>
            <div className={`event-container__${theme}__button__label`}>
              <FormattedMessage id={actuallyLabel()} />
            </div>
            {!isComponentVisible
            && actuallyCity
            && (
            <Button
              content={actuallyCity.Ciudad}
              onClick={() => setIsComponentVisible(true)}
            />
            )}
            <div ref={ref} style={{ display: isComponentVisible ? 'block' : 'none', zIndex: 2 }}>
              <EventListCity setVisible={() => setIsComponentVisible(false)} visible={isComponentVisible} />
            </div>
          </div>
        </div>
        <>
          {eventsNearBy && concat(content.slice(0, max),
            <div key={uniqueId()} className="last-child-card" style={{ width: widthLabel }} />)}
        </>
      </div>
      {visibleMoreSugestion
      && (
        <div className="event-container__button">
          <Button
            className="button-principal__event"
            content={button}
            width={sizeWindow.width <= 722 ? 5 : 10.5}
            onClick={() => sugestion()}
          />
        </div>
      )}
    </div>

  );
};
export default Event;
