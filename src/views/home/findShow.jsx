import React from 'react';
import { FormattedMessage } from 'react-intl';

import { Button, FindShowItem } from '@/components';
import { getDarkMode } from '@/helpers';
import { getRedux } from '@/redux/selectors';
import { keyLocationActuallyCity } from '@/redux/selectors/selectorsKey';
import src1 from '@/static/images/find-show-2.jpg';
import src2 from '@/static/images/find-show-1.jpg';

const FindShow = () => {
  const { color } = getDarkMode();
  const actuallyCity = getRedux(keyLocationActuallyCity);

  const findShow = () => null;
  const requestShow = () => null;

  return (
    <div className="find-show-container">
      <div className="find-show-container__content">
        <div className="find-show-container__content__header">
          <span className="find-show-container__content__header-title" style={{ color }}>
            <FormattedMessage
              id="show.view.in"
              values={{ city: actuallyCity ? actuallyCity.Ciudad : '' }}
            />
          </span>
          <p className="find-show-container__content__header-legend">
            <FormattedMessage id="show.view.legend" />
          </p>
        </div>
        <div className="find-show-container__content__image">
          <FindShowItem src={src1} onClick={requestShow} />
          <FindShowItem src={src2} onClick={requestShow} />
        </div>
      </div>
      <div className="find-show-container__button">
        <div className="find-show-container__button-title" style={{ color }}>
          <FormattedMessage id="show.view.button.legend" />
        </div>
        <Button
          className="button-principal__find-show"
          content={<FormattedMessage id="show.view.button.label" />}
          onClick={findShow}
        />
      </div>
    </div>
  );
};
export default FindShow;
