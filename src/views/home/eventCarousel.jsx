import React from 'react';
import { uniqueId } from 'lodash';
import PropTypes from 'prop-types';
import Carousel from '@brainhubeu/react-carousel';

import { MultiCarouselItem } from '@/components';
import config from '@/config';
import { getSizeWindow, RedirectTo, setDataMulticarousel } from '@/helpers';
import { eventPoster } from '@/redux/api/urls';
import { getRedux } from '@/redux/selectors';
import { keyVerticalCarousel } from '@/redux/selectors/selectorsKey';

const Arrow = ({ name }) => (
  <button
    className={`button-principal__multicarousel__${name}`}
    id={uniqueId()}
    name={name}
    aria-label="button"
    type="button"
  />
);
Arrow.propTypes = {
  name: PropTypes.string.isRequired,
};
const EventCarousel = () => {
  const [autoPlay, setAutoPlay] = React.useState(4500);
  const [animationSpeed, setanimationSpeed] = React.useState(1000);
  const { minItemsMultiCarousel } = config;
  const sizeWindow = getSizeWindow();
  React.useEffect(() => {
    const timer = setTimeout(() => {
      setAutoPlay(4500);
      setanimationSpeed(1000);
    }, 100);
    return () => {
      clearTimeout(timer);
      setAutoPlay(null);
      setanimationSpeed(null);
    };
  }, [sizeWindow.width]);
  const {
    centered, itemWidth, width, height, slidesPerPage, arrows,
  } = setDataMulticarousel(sizeWindow.width);
  const events = getRedux(keyVerticalCarousel);
  const items = events ? events.map((item) => (
    <MultiCarouselItem
      key={item.EventoId}
      src={eventPoster(item.EventoId)}
      location={item.Venue}
      title={item.EventoNombre}
      onclick={() => RedirectTo(item.Url)}
      onTouchstart={() => setanimationSpeed(null)}
      onTouchEnd={() => setanimationSpeed(1000)}
      width={width}
      height={height}
    />
  )) : null;
  if (events && minItemsMultiCarousel > events.length) {
    return null;
  }
  return (
    <div className="multi-carousel-container">
      <Carousel
        addArrowClickHandler
        animationSpeed={animationSpeed}
        arrows={arrows}
        arrowLeft={<Arrow name="arrow-left" />}
        arrowRight={<Arrow name="arrow-right" />}
        autoPlay={autoPlay}
        centered={centered}
        infinite
        itemWidth={itemWidth}
        keepDirectionWhenDragging
        slidesPerPage={slidesPerPage}
      >
        {events && items}
      </Carousel>
    </div>
  );
};

export default EventCarousel;
