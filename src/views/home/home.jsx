import React from 'react';

import config from '@/config';
import TkPlay from './tkPlay';
import Event from './event';
import FindShow from './findShow';
import Banner from './banner';
import EventCarousel from './eventCarousel';

const Home = () => {
  const { section } = config;
  const isVisibeSection = section.map((item) => item.visible);
  return (
    <>
      <div>
        { isVisibeSection[0] && <Banner /> }
        <div className="view-container">
          <div className="view-container__with-margin">
            { isVisibeSection[1] && <EventCarousel /> }
            { isVisibeSection[2] && <Event /> }
            {isVisibeSection[3] && <FindShow /> }
          </div>
        </div>
        { isVisibeSection[4] && <TkPlay /> }
      </div>
    </>
  );
};

export default Home;
