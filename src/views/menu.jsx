import React from 'react';

import { iconMenuClose, colors } from '@/model';
import { Button, ContentMenu, IconSvg } from '@/components';
import { getDarkMode, getSizeWindow, UseComponentVisible } from '@/helpers';

const Menu = () => {
  const { theme } = getDarkMode();
  const { height } = getSizeWindow();
  const { ref, isComponentVisible, setIsComponentVisible } = UseComponentVisible(false);
  let color;
  let background;
  if (theme === 'dark') {
    color = colors.fourthColor;
    background = colors.charcoalGrey;
  } else {
    color = colors.charcoalGrey;
    background = colors.fourthColor;
  }
  const display = isComponentVisible ? 'block' : 'none';
  return (
    <div className="h-100 menu-container" ref={ref}>
      <div>
        <Button
          className="button-principal__menu"
          height={2}
          width={4}
          content={<IconSvg fill={color} stroke={color} {...iconMenuClose} />}
          onClick={() => setIsComponentVisible(!isComponentVisible)}
        />
      </div>
      <div

        className="h-100 menu-container__content"
        style={{
          display,
          background,
          height: `${height}px`,
        }}
      />
      <ContentMenu
        onClick={() => setIsComponentVisible(!isComponentVisible)}
        visible={isComponentVisible}
      />
    </div>
  );
};

export default Menu;
