import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Image } from 'react-bootstrap';

import { ItemBrandResource, GoTo } from '@/components';
import { getDarkMode, setHeightView, getSizeWindow } from '@/helpers';
import { colors } from '@/model';
import { GET_BRAND_TICKETMUNDO, GET_BRAND_TICKETPLAY } from '@/redux/api/urls';
import { getRedux } from '@/redux/selectors';
import { keyAcceptCookie } from '@/redux/selectors/selectorsKey';
import brand from '@/static/images/brand-resources/brand-icon.png';
import play from '@/static/images/brand-resources/play-icon.svg';
import playdark from '@/static/images/brand-resources/play-icon-dark.svg';
import tkIcon from '@/static/images/logo/light/logo-eeuu.svg';
import tkIconDark from '@/static/images/logo/dark/logo-eeuu.svg';

const BrandResources = () => {
  const { height, width } = getSizeWindow();
  const heightLocal = getRedux(keyAcceptCookie);
  const heightActual = setHeightView(height, width);
  const { theme } = getDarkMode();
  const classNameItems = width > 719 ? 'd-flex' : 'd-flex flex-column align-items-center';
  const compare = theme === 'light';
  const iconTk = compare ? tkIcon : tkIconDark;
  const iconPlay = compare ? play : playdark;
  const color = compare ? colors.charcoalGrey : colors.fourthColor;
  return (
    <div className="view-container">
      <div className="view-container__with-margin">
        <div className="brand-resources-container" style={{ minHeight: heightActual }}>
          <GoTo to="/" />
          <div className="brand-resources-container__title" style={{ color }}>
            <span className="brand-resources-container__title-title">
              <FormattedMessage id="brand.resources.title" />
              <Image src={brand} className="brand-resources-container__title-image" alt="hands" />
            </span>
            <span className="brand-resources-container__title-subtitle">
              <FormattedMessage id="brand.resources.subtitle" />
            </span>
          </div>
          <div className={`${classNameItems} brand-resources-container__content-images`}>
            <ItemBrandResource src={iconTk} size="16.5" name="brand.resources.ticketmundo" downloadLink={GET_BRAND_TICKETMUNDO} />
            <ItemBrandResource src={iconPlay} size="16.5" name="brand.resources.play" downloadLink={GET_BRAND_TICKETPLAY} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default BrandResources;
