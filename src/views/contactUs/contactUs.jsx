import React from 'react';
import { Image } from 'react-bootstrap';
import { uniqueId, concat } from 'lodash';
import { useDispatch } from 'react-redux';
import { FormattedMessage } from 'react-intl';

import {
  ItemContactUs, Input, Button, GoTo,
} from '@/components';
import config from '@/config';
import {
  colors, patternEmailValidate, patternPhoneValidate, contactUsItems,
} from '@/model';
import { getDarkMode, validate } from '@/helpers';
import { setAction } from '@/redux/actions';
import { getRedux } from '@/redux/selectors';
import { keyActuallyItemOpenContactUs } from '@/redux/selectors/selectorsKey';
import { ACTUALLY_OPEN_ITEM_CONTACT_US } from '@/redux/types';
import hands from '@/static/images/contact-us/hand-icon.png';
import telegram from '@/static/images/contact-us/telegram-icon.png';
import telephone from '@/static/images/contact-us/phone-icon.png';
import Success from './success';

const ContactUs = () => {
  const dispatch = useDispatch();
  const { contactUsNumberPhone } = config;
  const { theme } = getDarkMode();
  const isOpen = getRedux(keyActuallyItemOpenContactUs);
  const color = theme === 'light' ? colors.charcoalGrey : colors.fourthColor;
  const [name, setName] = React.useState(null);
  const [email, setEmail] = React.useState(null);
  const [phone, setPhone] = React.useState(null);
  const [validateEmail, setValidateEmail] = React.useState(false);
  const [validatePhone, setValidatePhone] = React.useState(false);
  const [validateName, setValidateName] = React.useState(false);
  const [validateSubject, setValidateSubject] = React.useState(false);
  const [subject, setSubject] = React.useState(null);
  const [visible, setVisible] = React.useState(false);
  const isValid = (validate(email, patternEmailValidate)
    && validate(phone, patternPhoneValidate)
    && name && name.length > 2 && subject && subject.length > 2 && phone.length > 8);
  const onSend = () => {
    setValidateEmail(!validate(email, patternEmailValidate));
    setValidatePhone(!validate(phone, patternPhoneValidate));
    setValidateName(name === null);
    setValidateSubject(subject === null);
    if (isValid) {
      setVisible(true);
      // TODO hacer el dispatch para enviar el correo aqui
    }
  };
  React.useEffect(() => {
    if (isOpen !== null) {
      dispatch(setAction(ACTUALLY_OPEN_ITEM_CONTACT_US, null));
    }
  });
  let backgroundColor = colors.turquoiseBlue;
  if (!isValid) {
    backgroundColor = theme === 'light' ? colors.seventeenthColor : colors.fourteenthColor;
  }
  const content = contactUsItems.map((it) => (
    <ItemContactUs key={uniqueId()} src={it.src} title={it.title} content={it.content} to={it.to} />
  ));
  const lastItem = (<div key={uniqueId()} className="item-contactus-container last-child-card" />);
  return (
    <div className="view-container">
      <div className="view-container__with-margin">
        <div className="contact-us-container" style={{ display: visible ? 'none' : 'block' }}>
          <GoTo to="/" />
          <div className="contact-us-container__title">
            <span className="contact-us-container__title-label" style={{ color }}>
              <FormattedMessage id="contactus.title" />
              <Image src={hands} className="contact-us-container__title-label-image" alt="hands" />
            </span>
          </div>
          <div className="contact-us-container__icons">
            {concat(content, lastItem)}
          </div>
          <div className="contact-us-container__title contact-us-container__title-form">
            <span className="contact-us-container__title-label" style={{ color }}>
              <FormattedMessage id="contactus.writeus" />
              <Image src={telegram} className="contact-us-container__title-label-image" />
            </span>
          </div>
          <div className="contact-us-container__form">
            <Input
              autoFocus
              className="input-container input-container-contact-us"
              color={color}
              onChange={(e) => setName(e.target.value)}
              placeholder="name"
            />
            {validateName && (
              <span className="contact-us-container__form-message-error">
                <FormattedMessage
                  id="name.invalid.empty"
                />
              </span>
            )}
            <Input
              className="input-container input-container-contact-us"
              color={color}
              onChange={(e) => setEmail(e.target.value)}
              placeholder="email"
            />
            {validateEmail && (
              <span className="contact-us-container__form-message-error">
                <FormattedMessage
                  id="email.invalid.pattern"
                />
              </span>
            )}
            <Input
              className="input-container input-container-contact-us"
              color={color}
              onChange={(e) => setPhone(e.target.value)}
              placeholder="phone"
              type="number"
            />
            {validatePhone && (
              <span className="contact-us-container__form-message-error">
                <FormattedMessage
                  id="phone.invalid.pattern"
                />
              </span>
            )}
            <Input
              className="input-container input-container-contact-us"
              color={color}
              onChange={(e) => setSubject(e.target.value)}
              placeholder="subject"
            />
            {validateSubject && (
              <span className="contact-us-container__form-message-error mb-2">
                <FormattedMessage
                  id="subject.invalid.empty"
                />
              </span>
            )}
            <Button
              disabled={!isValid}
              className={isValid ? 'button-principal__contact-us-submit' : 'button-principal__contact-us-submit-disabled'}
              onClick={() => onSend()}
              style={{ backgroundColor }}
              content={<FormattedMessage id="send" />}
            />
          </div>
          <div className="contact-us-container__contact-phone">
            <Image src={telephone} className="contact-us-container__contact-phone-image" alt="phone contact us" />
            <div className="contact-us-container__contact-phone-content">
              <span className="contact-us-container__contact-phone-content-title d-block" style={{ color }}>
                {contactUsNumberPhone}
              </span>
              <span className="contact-us-container__contact-phone-content-content d-block">
                <FormattedMessage id="contactus.attention" />
              </span>
            </div>
          </div>
        </div>
        <div style={{ display: !visible ? 'none' : 'flex', paddingTop: '5rem' }}>
          <GoTo to="/" />
          <Success color={color} name={name || 'empty'} />
        </div>
      </div>
    </div>
  );
};
export default ContactUs;
