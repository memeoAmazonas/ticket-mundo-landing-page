import React from 'react';
import { filter, uniqueId } from 'lodash';
import { useParams, Redirect } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import { Image } from 'react-bootstrap';

import { GoTo } from '@/components';
import { getSizeWindow, setHeightView } from '@/helpers';
import { getRedux } from '@/redux/selectors';
import { keyAcceptCookie } from '@/redux/selectors/selectorsKey';
import { detailContactUs, idsDetailContactUs } from '@/model';
import ListAccordion from './listAccordion';

const DetailContact = () => {
  const { detail } = useParams();
  const { height, width } = getSizeWindow();
  const heightLocal = getRedux(keyAcceptCookie);
  const heightActual = setHeightView(height, width);
  const actual = filter(detailContactUs, (it) => it.id === detail);
  if (!idsDetailContactUs.includes(detail) || actual.length === 0) {
    return <Redirect to="/contact-us" />;
  }
  return (
    <div className="view-container">
      <div className="view-container__with-margin">
        <div className="detail-contact-us-container" style={{ minHeight: heightActual }}>
          <GoTo to="/contact-us" />
          <div className="detail-contact-us-container__title">
            <div className="detail-contact-us-container__title-label">
              <FormattedMessage id={actual[0].label} />
            </div>
            <Image alt={`${uniqueId()}-contact-us-detail`} src={actual[0].icon} className="detail-contact-us-container__title-icon" />
          </div>
          <div className="detail-contact-us-container-subtitle">
            <FormattedMessage id={actual[0].subtitle} />
          </div>
          <ListAccordion items={actual[0].items} />
        </div>
      </div>
    </div>
  );
};
export default DetailContact;
