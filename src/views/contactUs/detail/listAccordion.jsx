import React from 'react';
import { uniqueId } from 'lodash';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Accordion, Card, Image } from 'react-bootstrap';

import { ToogleButton } from '@/components';
import { getDarkMode } from '@/helpers';
import { colors } from '@/model';
import { getRedux } from '@/redux/selectors';
import { keyActuallyItemOpenContactUs } from '@/redux/selectors/selectorsKey';
import down from '@/static/images/contact-us/detail/arrow-down.svg';
import up from '@/static/images/contact-us/detail/arrow-up.svg';

const ListAccordion = ({ items }) => {
  const { theme } = getDarkMode();
  const isOpen = getRedux(keyActuallyItemOpenContactUs);
  const setColor = (index) => {
    if (index === isOpen) {
      return colors.turquoiseBlue;
    }
    if (theme === 'light') {
      return colors.itemContactUs;
    }
    return colors.fourthColor;
  };
  const setIcon = (index) => {
    if (index === isOpen) {
      return up;
    }
    return down;
  };
  const content = items.map((it, index) => (
    <Card key={uniqueId()} bsPrefix="list-accordion-content">
      <Card.Header>
        <ToogleButton eventKey={index}>
          <div className="d-flex justify-content-between">
            <div className="class-2">
              <div className="list-accordion-content__header-icon" style={{ backgroundColor: setColor(index) }} />
              <span className="list-accordion-content__header-title" style={{ color: setColor(index) }}>
                <FormattedMessage id={it.label} />
              </span>
            </div>
            <div className="d-flex align-items-center justify-content-end">
              <Image src={setIcon(index)} className="list-accordion-content__header-close-icon" />
            </div>
          </div>
        </ToogleButton>
      </Card.Header>
      <Accordion.Collapse eventKey={index}>
        <Card.Body>
          <p className="list-accordion-content__body">
            <FormattedMessage id={it.content} />
          </p>
          {it.content1
          && (
            <p className="list-accordion-content__body-alt">
              <FormattedMessage id={it.content1} />
            </p>
          )}
        </Card.Body>
      </Accordion.Collapse>
    </Card>
  ));
  return (
    <Accordion defaultActiveKey={isOpen}>
      {content}
    </Accordion>
  );
};
ListAccordion.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    content: PropTypes.string,
  })).isRequired,
};
export default ListAccordion;
