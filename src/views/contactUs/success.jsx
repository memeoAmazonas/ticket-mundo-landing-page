import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import { getSizeWindow, setHeightView } from '@/helpers';
import { getRedux } from '@/redux/selectors';
import { keyAcceptCookie } from '@/redux/selectors/selectorsKey';
import boxMail from '@/static/images/contact-us/box-mail-icon.png';

const Success = ({ name, color }) => {
  const { height, width } = getSizeWindow();
  const heightLocal = getRedux(keyAcceptCookie);
  const heightActual = setHeightView(height, width);
  const padding = `${heightActual * 0.05}px 0`;
  return (
    <div className="w-100 d-flex align-items-center" style={{ minHeight: heightActual, padding }}>
      <div className="succes-container">
        <div className="succes-container_label" style={{ color }}>
          <FormattedMessage id="contactus.success.name" values={{ name }} />
        </div>
        <div className="succes-container_label" style={{ color }}>
          <FormattedMessage id="contactus.success.content" />
          <img src={boxMail} alt="box-mail" className="contact-us-container__title-label-image-success" />
        </div>
        <div className="succes-container_sub-label">
          <FormattedMessage id="contactus.success.subcontent" />
        </div>
      </div>
    </div>
  );
};

Success.propTypes = {
  color: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
};
export default Success;
