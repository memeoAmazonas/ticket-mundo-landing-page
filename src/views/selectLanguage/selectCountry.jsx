import React from 'react';
import { useDispatch } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { uniqueId, concat } from 'lodash';

import config from '@/config';
import Countries from '@/model/countries';
import { SetVisibleModalInitCountrySelected } from '@/redux/actions';
import { getCountry, getDarkMode, RedirectTo } from '@/helpers';
import srcLight from '@/static/images/logo/light/logo-eeuu.svg';
import srcDark from '@/static/images/logo/dark/logo-eeuu.svg';
import ItemSelectCountry from '@/components/items/itemSelectCountry';

const SelectCountry = () => {
  const dispatch = useDispatch();
  const { countryCode } = config;
  const { theme } = getDarkMode();
  const onSelected = (code, url) => {
    if (!getCountry(code, countryCode)) {
      RedirectTo(url);
      return;
    }
    dispatch(SetVisibleModalInitCountrySelected(false));
    sessionStorage.setItem('country', true);
  };
  const listCountries = Countries.map((item) => (
    <ItemSelectCountry
      key={item.name}
      name={item.name}
      link={item.link.replace('/#', '')}
      src={item.src}
      onClick={() => onSelected(item.countryCode, item.link)}
      theme={theme}
    />
  ));
  const lastItem = (
    <div key={uniqueId()} className="item-select-country-container-last-item" />
  );
  const src = theme === 'light' ? srcLight : srcDark;
  return (
    <div className="view-container">
      <div className="view-container__with-margin">
        <div className="select-country-container">
          <div className="d-flex justify-content-center">
            <img src={src} alt="tiketmundo site" />
          </div>
          <span className="select-country-container__title"><FormattedMessage id="selectOtherCountry" /></span>
          <div className="select-country-container__content">
            { concat(listCountries, lastItem) }
          </div>
        </div>
      </div>
    </div>
  );
};
export default SelectCountry;
