import React from 'react';
import { IntlProvider } from 'react-intl';
import { useDispatch } from 'react-redux';
import { SkeletonTheme } from 'react-loading-skeleton';

import config from '@/config';
import strings from '@/customization/strings';
import colors from '@/model/colors';
import { hourSleep } from '@/model/constants';
import {
  keyLanguage, keyLocationInfoCountry,
  keyLocationCityList, keySrc, keyIsVisibleSearch,
  keyLoadingSkeleton, keyVerticalCarousel,
  keyNearByCarousel, keyVisibleLanguageList,
  keyModalCountryInit,
} from '@/redux/selectors/selectorsKey';
import {
  GetListCity, SetInfoCountry, SetLocale, ChangeLogo, SetLoadingSkeleton,
  GetVerticalCarousel, GetEventNearBy, SetLanguageVisibleAboutUs } from '@/redux/actions';

import Body from '@/views/layout/body';
import Footer from '@/views/layout/footer';
import SkeletonLoading from '@/views/skeletonLoading';
import {
  getSkeletonColor, getLogo, getCookie, setCookie, removeCookie, ScrollToTop,
} from '@/helpers';
import { getRedux } from '@/redux/selectors';
import HeaderAndCookies from '../header/headerAndCookies';

const Page = () => {
  const color = getSkeletonColor();
  const dispatch = useDispatch();
  const defaultTheme = !hourSleep.includes(new Date().getHours()) ? 'dark' : 'light';
  const infoCountry = getRedux(keyLocationInfoCountry);
  const cityList = getRedux(keyLocationCityList);
  const isVisibleSearch = getRedux(keyIsVisibleSearch);
  const language = getRedux(keyLanguage);
  const loadingSkeleton = getRedux(keyLoadingSkeleton);
  const srcLogo = getRedux(keySrc);
  const events = getRedux(keyVerticalCarousel);
  const eventsNearBy = getRedux(keyNearByCarousel);
  const langABUList = getRedux(keyVisibleLanguageList);
  const visibleCountry = getRedux(keyModalCountryInit);
  React.useEffect(() => {
    if (!infoCountry) {
      dispatch(SetInfoCountry());
      removeCookie('language');
    }
  });
  React.useEffect(() => {
    if (!events) {
      dispatch(GetVerticalCarousel());
    }
  });
  React.useEffect(() => {
    if (infoCountry) {
      if (!eventsNearBy) {
        const { latitude, longitude } = infoCountry;
        dispatch(GetEventNearBy({
          latitude,
          longitude,
        }));
      }
    }
  });
  React.useEffect(() => {
    if (!getCookie('language')) {
      setCookie('language', config.defaultLang);
      dispatch(SetLocale(config.defaultLang));
    } else if (!language) {
      dispatch(SetLocale(getCookie('language')));
    }
  });
  React.useEffect(() => {
    if (!cityList) {
      dispatch(GetListCity());
    }
  });
  React.useEffect(() => {
    if (!srcLogo) {
      if (defaultTheme !== 'dark') {
        sessionStorage.setItem('theme', 'dark');
        document.body.classList.add('__darkmode');
        const logo = getLogo(true);
        dispatch(ChangeLogo(logo));
      } else {
        sessionStorage.setItem('theme', 'light');
        document.body.classList.remove('__darkmode');
        const logo = getLogo(false);
        dispatch(ChangeLogo(logo));
      }
    }
  });
  React.useEffect(() => {
    if (events && infoCountry && eventsNearBy) {
      dispatch(SetLoadingSkeleton(false));
    }
  });
  React.useEffect(() => {
    if (!langABUList) {
      dispatch(SetLanguageVisibleAboutUs(false));
    }
  });
  const higlightColor = defaultTheme !== 'light' ? colors.white : colors.fourteenthColor;

  return (
    <div className="page-container h-100">
      <IntlProvider
        locale={language || config.defaultLang}
        messages={strings[language || config.defaultLang]}
      >
        <SkeletonTheme color={color} highlightColor={higlightColor}>
          {loadingSkeleton && <SkeletonLoading />}
          {loadingSkeleton === false && language
        && (
          <>
            <ScrollToTop />
            { !visibleCountry && <HeaderAndCookies /> }
            <Body />
            {!isVisibleSearch && !visibleCountry && <Footer disabled={config.section[5].visible} />}
          </>
        )}
        </SkeletonTheme>
      </IntlProvider>
    </div>
  );
};
export default Page;
