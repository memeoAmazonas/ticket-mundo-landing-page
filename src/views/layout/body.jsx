import React, { useEffect } from 'react';
import { filter, slice } from 'lodash';
import { Switch, Route } from 'react-router-dom';

import config from '@/config';
import {
  keyIsVisibleSearch,
  keyModalCountryInit,
  keyLocationInfoCountry,
} from '@/redux/selectors/selectorsKey';
import { SetFind, SetVisibleModalInitCountrySelected } from '@/redux/actions';
import { getRedux } from '@/redux/selectors';
import Search from '@/views/header/Search';
import { useDispatch } from 'react-redux';
import { getCountry } from '@/helpers';
import SelectCountry from '../selectLanguage/selectCountry';
import routes from '../../routes';

const Body = () => {
  const dispatch = useDispatch();
  const { countryCode } = config;
  const isVisibleSearch = getRedux(keyIsVisibleSearch);
  const visibleCountry = getRedux(keyModalCountryInit);
  const infoCountry = getRedux(keyLocationInfoCountry);
  const [isVisibleCountry, setIsVisibleCountry] = React.useState(true);
  useEffect(() => {
    if (isVisibleSearch === undefined) {
      dispatch(SetFind(false));
    }
  });
  const isRefresh = () => filter(slice(routes, 1),
    (item) => window.location.href.includes(item.path));
  isRefresh();
  useEffect(() => {
    if (isVisibleCountry) {
      if (!window.location.href.includes('#') && !isRefresh().length > 0) {
        if (infoCountry && !visibleCountry) {
          const isOpenModal = sessionStorage.country;
          if (getCountry(infoCountry.country_code, countryCode)) {
            dispatch(SetVisibleModalInitCountrySelected(false));
          } else if (!isOpenModal) {
            dispatch(SetVisibleModalInitCountrySelected(true));
          }
        }
      } else {
        dispatch(SetVisibleModalInitCountrySelected(false));
      }
      setIsVisibleCountry(false);
    }
  }, [isVisibleCountry, infoCountry, visibleCountry, countryCode, dispatch]);
  if (visibleCountry) {
    return (
      <div>
        <Route path="*" component={SelectCountry} />
      </div>
    );
  }
  return (
    <div>
      <Switch>
        {routes
          .map((item) => (
            <Route
              key={item.path}
              exact
              path={item.path}
              component={isVisibleSearch
                ? Search : item.component}
            />
          ))}
      </Switch>
    </div>
  );
};
export default Body;
