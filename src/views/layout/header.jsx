import React from 'react';
import { Link } from 'react-router-dom';

import { Image, Separator, SearchIcon } from '@/components';
import { getDarkMode, getSizeWindow, useDocumentScrollThrottled } from '@/helpers';
import { colors } from '@/model';
import { keySrc } from '@/redux/selectors/selectorsKey';
import { getRedux } from '@/redux/selectors';
import Login from '@/views/login';
import Menu from '@/views/menu';

const Header = () => {
  const [shouldHideHeader, setShouldHideHeader] = React.useState(false);
  useDocumentScrollThrottled((callbackData) => {
    const { currentScrollTop } = callbackData;

    setTimeout(() => {
      setShouldHideHeader(currentScrollTop > 0);
    }, 200);
  });

  const onScroll = shouldHideHeader ? 'on-scroll' : '';

  const { theme } = getDarkMode();
  const { width } = getSizeWindow();
  const src = getRedux(keySrc);
  const background = theme === 'light' ? colors.fourthColor : colors.seventhColor;
  // Para volver a mostrar el login se debe quitar la condicion width <= 590 por { menu }
  const menu = width > 590 ? (
    <>
      <Separator width={1} />
      <Login src={src} />
    </>
  ) : <Menu src={src} />;
  return (
    <nav className={`nav-bar-container ${onScroll}`} style={{ background }}>
      <div className="nav-bar-container__content view-container__with-margin">
        <div className="nav-bar-container__content__logo">
          <Link to="/">
            <Image src={src} height={2.6} width={19} />
          </Link>
        </div>
        <div className="nav-bar-container__content__menu">
          <SearchIcon />
          {/*
          TODO se oculto el menu
           width <= 590 && menu
           */}
        </div>
      </div>
    </nav>
  );
};
export default Header;
