import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';

import {
  Button, CountryList, Image, SocialNetworkIcon,
} from '@/components';
import { colors, SocialMediaList } from '@/model';
import { getDarkMode } from '@/helpers';
import logo from '@/static/images/logo-principal-footer.svg';
import AboutUs from '../home/aboutUs';

const Footer = ({ disabled }) => {
  const { theme } = getDarkMode();
  const backgroundColor = theme !== 'light' ? colors.seventhColor : colors.black;
  const socialMedia = SocialMediaList.map((item) => (
    <SocialNetworkIcon
      key={item.link}
      name={item.name}
      url={item.link}
      borderWidth="0"
      size="17"
      iconSize="14"
    />
  ));
  const internal = (url) => {
  };
  return (
    <>
      {disabled && <AboutUs /> }
      <div className="footer-container" style={{ backgroundColor }}>
        <div className="footer-container__content view-container__with-margin">
          <div className="footer-container__content__country-list">
            <CountryList backgroundColor={backgroundColor} />
          </div>
          <div className="footer-container__content__button-list">
            <Link to="/">
              <Button
                className="button-principal__footer-label"
                content={<FormattedMessage id="privacyPolice" />}
                onClick={null}
              />
            </Link>
            <Button
              className="button-principal__footer-label"
              content={<FormattedMessage id="termsUse" />}
              onClick={() => internal('terms-use')}
            />
          </div>
          <div className="footer-container__content__social-media-list">
            {socialMedia}
          </div>
          <div className="footer-container__content__copyright">
            <span className="footer-container__content__copyright-label">
              <FormattedMessage id="nameApp.footer" values={{ value: '© ' }} />
            </span>
            <span className="footer-container__content__copyright-date">
              {`  ${new Date().getFullYear()}`}
            </span>
            <Image src={logo} width={14.8} height={1.72} />
          </div>
        </div>
      </div>
    </>
  );
};

Footer.propTypes = {
  disabled: PropTypes.bool,
};
Footer.defaultProps = {
  disabled: false,
};
export default Footer;

/*
TODO: contenido del header se quito de manera provicional para mostrar el contact us cuando
        <div className="footer-container__content__button-list">
          <Link to="/">
            <Button
              className="button-principal__footer-label"
              content={<FormattedMessage id="privacyPolice" />}
              onClick={null}
            />
          </Link>
          <Button
            className="button-principal__footer-label"
            content={<FormattedMessage id="termsUse" />}
            onClick={() => internal('terms-use')}
          />
                  </div>

                            <Link to="/contact-us">
            <Button
              className="button-principal__footer-label"
              content={<FormattedMessage id="contactUs" />}
            />
          </Link>
 */
