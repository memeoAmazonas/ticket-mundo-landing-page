import React from 'react';
import Skeleton from 'react-loading-skeleton';

import getSizeWindow from '@/helpers/getSizeWindow';
import setNumberItem from '@/helpers/setNumberItem';

const SkeletonLoading = () => {
  const size = getSizeWindow();
  const multiCarousel = Array
    .from({ length: setNumberItem(size, 4, 4, 3, 3) },
      (_, idx) => `${idx}`)
    .map((item) => (
      <div className="skeleton-loading-container__content__center__multi-carousel__item" key={item}>
        <Skeleton />
      </div>
    ));
  const events = Array
    .from({ length: setNumberItem(size, 9, 9, 8, 4) },
      (_, idx) => `${idx}`)
    .map((item) => (
      <div className="skeleton-loading-container__content__center__event__item" key={item}>
        <div className="skeleton-loading-container__content__center__event__item__image">
          <Skeleton />
        </div>
        <div className="skeleton-loading-container__content__center__event__item__content">
          <div className="skeleton-loading-container__content__center__event__item__content__label">
            <Skeleton />
          </div>
          <div className="skeleton-loading-container__content__center__event__item__content__date">
            <Skeleton />
          </div>
        </div>
      </div>
    ));
  return (
    <div className="skeleton-loading-container">
      <div className="skeleton-loading-container__content">
        <div className="skeleton-loading-container__content__icon">
          <Skeleton />
        </div>
        <div className="skeleton-loading-container__content__menu">
          <Skeleton />
        </div>

      </div>
      <div className="skeleton-loading-container__carousel">
        <Skeleton />
      </div>
      <div className="skeleton-loading-container__content">
        <div className="skeleton-loading-container__content__center">
          <div className="skeleton-loading-container__content__center__multi-carousel">
            {multiCarousel}
          </div>
          <div className="skeleton-loading-container__content__center__find-event">
            <Skeleton />
          </div>
          <div className="skeleton-loading-container__content__center__event">
            {events}
          </div>
          <div className="skeleton-loading-container__content__center__event-city">
            <Skeleton />
            <div className="skeleton-loading-container__content__center__event-city__altern">
              <Skeleton />

            </div>
          </div>
          <div className="skeleton-loading-container__content__center__request-content">
            <div className="skeleton-loading-container__content__center__request-content__left">
              <div className="skeleton-loading-container__content__center__request-content__left__item">
                <Skeleton />
              </div>
              <div className="skeleton-loading-container__content__center__request-content__left__item">
                <Skeleton />
              </div>
            </div>
            <div className="skeleton-loading-container__content__center__request-content__right">
              <div className="skeleton-loading-container__content__center__request-content__right-first">
                <Skeleton />
              </div>
              <div className="skeleton-loading-container__content__center__request-content__right-first">
                <Skeleton />
              </div>
              <div className="skeleton-loading-container__content__center__request-content__right-middle">
                <Skeleton />
              </div>
              <div className="skeleton-loading-container__content__center__request-content__right-last">
                <Skeleton />
              </div>
            </div>
          </div>
          <div className="skeleton-loading-container__content__center__play">
            <div className="skeleton-loading-container__content__center__play__left">
              <div className="skeleton-loading-container__content__center__play__left-first">
                <Skeleton />
              </div>
              <div className="skeleton-loading-container__content__center__play__left-second">
                <Skeleton />
              </div>
              <div className="skeleton-loading-container__content__center__play__left-third">
                <Skeleton />
              </div>
              <div className="skeleton-loading-container__content__center__play__left-last">
                <Skeleton />
              </div>
            </div>
            <div className="skeleton-loading-container__content__center__play__item">
              <Skeleton />
            </div>
            <div className="skeleton-loading-container__content__center__play__item">
              <Skeleton />
            </div>
          </div>
        </div>
        <div className="skeleton-loading-container__content__footer">
          <Skeleton />
        </div>
      </div>
    </div>
  );
};

export default SkeletonLoading;
