import React from 'react';
import { FormattedMessage } from 'react-intl';

import { ItemSellWithUsContent } from '@/components';
import { getDarkMode } from '@/helpers';
import { colors } from '@/model';
import food from '@/static/images/sell-wit-us/food-bg.jpg';
import merchandising from '@/static/images/sell-wit-us/merchandising-bg.jpg';

const ComplementSell = () => {
  const { theme } = getDarkMode();
  const color = theme === 'light' ? colors.charcoalGrey : colors.fourthColor;
  return (
    <div className="container-complements">
      <div className="container-complements-title" style={{ color }}>
        <FormattedMessage id="sell.with.us.complements" />
      </div>
      <div className="container-complements-subtitle" style={{ color }}>
        <FormattedMessage id="sell.with.us.complements.subtitle" />
      </div>
      <div className="container-complements-image">
        <ItemSellWithUsContent
          src={food}
          color={color}
          content="sell.with.us.complements.item.content"
          title="sell.with.us.complements.item.title"
          type="complement"
        />
        <ItemSellWithUsContent
          src={merchandising}
          color={color}
          content="sell.with.us.complements.item1.content"
          title="sell.with.us.complements.item1.title"
          type="complement"
        />
      </div>
    </div>
  );
};
export default ComplementSell;
