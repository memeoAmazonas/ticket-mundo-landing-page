import React from 'react';
import { FormattedMessage } from 'react-intl';

import { ItemSellWithUsContent } from '@/components';
import { getDarkMode } from '@/helpers';
import { colors } from '@/model';
import seo from '@/static/images/sell-wit-us/seo-bg.svg';
import mailling from '@/static/images/sell-wit-us/mailling-bg.svg';
import data from '@/static/images/sell-wit-us/data-analysis-bg.svg';

const ContentPromotion = () => {
  const { theme } = getDarkMode();
  const color = theme === 'light' ? colors.charcoalGrey : colors.fourthColor;
  return (
    <div className="container-promotion">
      <div className="container-promotion-title" style={{ color }}>
        <FormattedMessage id="sell.with.us.sell.promotion.title" />
      </div>
      <div className="container-promotion-subtitle" style={{ color }}>
        <FormattedMessage id="sell.with.us.sell.promotion.subtitle" />
      </div>
      <div className="container-promotion-image">
        <ItemSellWithUsContent
          src={mailling}
          color={color}
          content="sell.with.us.promotion.item.content"
          title="sell.with.us.promotion.item.title"
          type="promotion"
        />
        <ItemSellWithUsContent
          src={data}
          color={color}
          content="sell.with.us.promotion.item1.content"
          title="sell.with.us.promotion.item1.title"
          type="promotion"
        />
        <ItemSellWithUsContent
          src={seo}
          color={color}
          content="sell.with.us.promotion.item2.content"
          title="sell.with.us.promotion.item2.title"
          type="promotion"
        />
        <div className="last-item-promotion" />
      </div>

    </div>
  );
};
export default ContentPromotion;
