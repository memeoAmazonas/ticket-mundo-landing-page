import React from 'react';
import { uniqueId } from 'lodash';

import { ItemSellWithUsCategory } from '@/components';
import { sellCategory } from '@/model';

const Categories = () => sellCategory.map((item) => (
  <ItemSellWithUsCategory key={uniqueId()} src={item.src} label={item.label} />
));
export default Categories;
