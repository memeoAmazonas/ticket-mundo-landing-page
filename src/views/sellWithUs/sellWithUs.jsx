import React from 'react';
import { FormattedMessage } from 'react-intl';

import { GoTo } from '@/components';
import { getDarkMode, getSizeWindow } from '@/helpers';
import { colors, itemListBroadcast, itemListSellWithUs } from '@/model';
import ticketIn from '@/static/images/sell-wit-us/ticketing-bg.png';
import play from '@/static/images/sell-wit-us/play-bg.png';
import Categories from './categories';
import ListImage from './ListImage';
import ComplementSell from './complementSell';
import ContentPromotion from './contentPromotion';

const SellWithUs = () => {
  const { width } = getSizeWindow();
  const { theme } = getDarkMode();
  const color = theme === 'light' ? colors.black : colors.fourthColor;
  const className = width < 723 ? 'bg-contain' : '';
  return (
    <div className="view-container">
      <div className="view-container__with-margin">
        <div className="sell-with-us-container">
          <GoTo to="/contact-us" />
          <span className="sell-with-us-container_title" style={{ color }}>
            <FormattedMessage id="sell.with.us.title" />
          </span>
          <div className="sell-with-us-container__list-category">
            <Categories />
          </div>
          <ListImage items={itemListSellWithUs} src={ticketIn} title="sell.with.us.sell.ticket" keyButton="sell.with.us.goto.tiketing" />
          <ComplementSell />
          <ListImage
            className={className}
            items={itemListBroadcast}
            keyButton="sell.with.us.goto.play"
            title="sell.with.us.sell.broadcast"
            src={play}
          />
          <ContentPromotion />
        </div>
      </div>
    </div>
  );
};

export default SellWithUs;
