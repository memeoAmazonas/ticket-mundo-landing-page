import React from 'react';
import { uniqueId } from 'lodash';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import { Button } from '@/components';
import { getDarkMode } from '@/helpers';
import { colors } from '@/model';

const ListImage = ({
  className, items, keyButton, onClick, src, title,
}) => {
  const { theme } = getDarkMode();
  const color = theme === 'light' ? colors.black : colors.fourthColor;
  const colorList = theme === 'light' ? colors.charcoalGrey : colors.blueyGrey;
  const list = items.map((item) => (
    <div key={uniqueId()} className="list-image-container__legeng__list-item">
      <div className="list-image-container__legeng__list-item-circle" style={{ backgroundColor: colorList }} />
      <span className="list-image-container__legeng__list-item-label" style={{ color: colorList }}>
        <FormattedMessage id={item} />
      </span>
    </div>
  ));
  return (
    <div className="list-image-container">
      <div className="list-image-container__legeng">
        <span className="list-image-container__legeng__title" style={{ color }}>
          <FormattedMessage id={title} />
        </span>
        <div className="list-image-container__legeng__list">
          {list}
        </div>
        <div className="list-image-container__legeng__button">
          <Button
            className="button-principal__list-image-sell"
            onClick={onClick}
            content={<FormattedMessage id={keyButton} />}
          />
        </div>
      </div>
      <div
        className={`list-image-container__image ${className}`}
        style={{ backgroundImage: `url(${src})` }}
      />
      <div className="list-image-container__button">
        <Button
          className="button-principal__list-image-sell"
          onClick={onClick}
          content={<FormattedMessage id={keyButton} />}
        />
      </div>
    </div>
  );
};
ListImage.propTypes = {
  className: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.string).isRequired,
  keyButton: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  src: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
};
ListImage.defaultProps = {
  className: '',
  onClick: null,
};

export default ListImage;
//  style={{ marginTop: `${(height * 0.14).toFixed(2)}rem` }}
