import {AboutUsItemCategory} from "./aboutUsItemCategory";

export interface AboutUsItem {
  label: string,
  items:  AboutUsItemCategory[],
}
