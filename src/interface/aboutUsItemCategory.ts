export interface AboutUsItemCategory {
  external: boolean,
  label: string,
  url:string,
}
