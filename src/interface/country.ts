export default interface Country {
  countryCode: string,
  name: string,
  link: string,
  src: string,
  width: number,
}
