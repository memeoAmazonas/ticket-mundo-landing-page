export interface Link {
  name: string,
  link: string,
  src: string,
}
