const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const BrotliPlugin = require('brotli-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const WorkboxPlugin = require('workbox-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');

const sourceDirectory = path.resolve(__dirname, 'src');
const outputDirectory = path.resolve(__dirname, 'dist');

module.exports = (env, argv) => {
  const isDevelopment = argv.mode !== 'production';
  return {
    mode: isDevelopment ? 'development' : 'production',
    entry: {
      app: `${sourceDirectory}/index.jsx`,
    },
    devtool: '',
    devServer: {
      contentBase: path.join(__dirname, './'),
      publicPath: '/',
      historyApiFallback: true,
      open: true,

    },
    output: {
      filename: isDevelopment ? 'js/[name].js' : 'js/[name].[hash].js',
      path: outputDirectory,
      publicPath: '/',
    },
    optimization: {
      runtimeChunk: 'single',
      splitChunks: {
        chunks: 'all',
        maxInitialRequests: Infinity,
        minSize: 0,
        cacheGroups: {
          vendor: {
            test: /[\\/]node_modules[\\/]/,
            name(module) {
              const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];
              return `npm.${packageName.replace('@', '')}`;
            },
          },
          styles: {
            name: 'styles',
            test: /\.scss$/,
            chunks: 'all',
            enforce: true,
          },
        },
      },
    },
    performance: {
      hints: false,
      maxEntrypointSize: 512000,
      maxAssetSize: 512000,
    },
    resolve: {
      extensions: ['.js', '.jsx', '.ts', '.tsx', '.scss', 'css', '.gif', '.png', '.jpg', '.jpeg', '.svg'],
      alias: {
        '@': sourceDirectory,
      },
    },
    module: {
      rules: [
        {
          test: /\.([tj])sx?$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
          },
        },
        {
          test: /\.scss$/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader,
            },
            'css-loader',
            {
              loader: 'sass-loader',
            },
          ],
        },
        {
          test: /\.(png|svg|jpg|jpeg|gif)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                outputPath: 'images',
              },
            },
          ],
        },
        {
          test: /\.(ttf)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                outputPath: 'fonts',
              },
            },
          ],
        },
      ],
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: `${sourceDirectory}/index.html`,
        filename: 'index.html',
        title: 'Ticketmundo',
      }),
      new MiniCssExtractPlugin({
        // filename: isDevelopment ? 'css/[name].css' : 'css/[name].[hash].css',
        chunkFilename: isDevelopment ? 'css/[name].css' : 'css/[name].[hash].css',
      }),
      new CleanWebpackPlugin({
        cleanOnceBeforeBuildPatterns: ['css/*.*', 'js/*.*', 'fonts/*.*', 'images/*.*'],
      }),
      new WorkboxPlugin.GenerateSW({
        exclude: [/\.(?:png|jpg|jpeg|svg)$/],
        runtimeCaching: [{
          urlPattern: /\.(?:png|jpg|jpeg|svg)$/,
          handler: 'CacheFirst',
          options: {
            cacheName: 'images',
            expiration: {
              maxEntries: 10,
            },
          },
        }],
      }),
      new BrotliPlugin({
        asset: '[path].br[query]',
        test: /\.(js|jsx|css|html|svg)$/,
        threshold: 10240,
        minRatio: 0.8,
      }),
      new ManifestPlugin(),
    ],
  };
};
